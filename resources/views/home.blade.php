@extends('layouts.app')

@section('content')
    <x-titulo_principal/>

    <div class="flex items-center justify-center mt-12">
        <div class="xbox_reward">
            <img src="{{ asset('images/premio_xbox.png') }}" alt="" srcset="">
        </div>
        <div class="gamepass_reward text-white md:ml-10">
            <img src="{{ asset('images/logo_xbox_alt.svg') }}" alt="">
            <div class="flex items-center justify-between mt-3">
                <span>14 días</span>
                <span>1 mes</span>
                <span>3 meses</span>
            </div>
        </div>
    </div><!-- Premios -->

    <div class="text-center text-2xl mt-10 font-industry text-white">
        <p>Responde las trivias y avanza de nivel, tú decides el premio</p>
        <p>final ¡Sólo los mejores podrán conseguir</p>
        <p>una consola xbox one x!</p>
    </div><!-- Dinamica -->

    <div class="text-center mt-10">
        <!--a class="text-black bg-x_green_lightest text-base px-24 py-5 inline-block font-industry" href="/registro">Regístrate aquí</a-->
        <x-input.link_button class="btn btn-xbox mx-auto" href="/registro" texto="Regístrate aquí"/>
        <p class="font-amsi mt-6 normal-case text-white">¿Ya eres usuario?, <a class="underline" href="{{ route('login') }}">Inicia sesión</a></p>
    </div><!-- Links -->

    <x-contador_premios />

@endsection
