@props([
	"premios" => contador_premios()
])

<div class="flex flex-row items-start mt-10 font-industry">
	
    <div class="w-1/4 text-center text-3xl text-x_green_lightest px-12">
        <p>{{ $premios[4] }}</p>
        <p>Consolas</p>
        <img class="mt-8" src="{{ asset('images/premio_xbox.png')}}" alt="">
    </div>
    <div class="w-1/4 text-center text-3xl text-x_green_lightest px-12">
        <p>{{ $premios[1] }}</p>
        <p>Códigos</p>
        <img class="mt-8" src="{{ asset('images/logo_xbox_alt.svg')}}" alt="">
        <p class="text-2xl text-white mt-2">14 días</p>
    </div>
    <div class="w-1/4 text-center text-3xl text-x_green_lightest px-12">
        <p>{{ $premios[2] }}</p>
        <p>Códigos</p>
        <img class="mt-8" src="{{ asset('images/logo_xbox_alt.svg')}}" alt="">
        <p class="text-2xl text-white mt-2">1 mes</p>
    </div>
    <div class="w-1/4 text-center text-3xl text-x_green_lightest px-12">
        <p>{{ $premios[3] }}</p>
        <p>Códigos</p>
        <img class="mt-8" src="{{ asset('images/logo_xbox_alt.svg')}}" alt="">
        <p class="text-2xl text-white mt-2">3 meses</p>
    </div>
</div><!-- Premios a repartir -->