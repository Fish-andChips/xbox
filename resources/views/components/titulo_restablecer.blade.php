@props([
	'hide'=> false
])
<div class="mt-12 mx-auto w-11/12 max-w-sm sm:max-w-lg md:max-w-xl text-center font-industry">
    <div class="py-3 bg-x_green_lightest">
        <h1 class="text-2xl sm:text-4xl md:text-5xl leading-none text-x_green">Restablecer contraseña</h1>
    </div>
    <div>
    @if(!$hide)
    	<p class="text-2xl sm:text-4xl md:text-5xl text-white">¡oh no! ¿olvidaste tu contraseña?</p>
    	<img class="mx-auto mt-4 w-10 md:w-16" src="{{ asset('images/lost_password.svg') }}" alt="">
    @endif
    </div>
</div>

