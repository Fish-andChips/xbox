@section('title', 'Date la vuelta con xbox - Mi perfil')

<div class="mt-12 mx-auto w-11/12 max-w-xs sm:max-w-lg md:max-w-xl text-center font-industry">
    <div class="py-3 bg-x_green_lightest">
        <h1 class="text-2xl sm:text-4xl md:text-5xl leading-none text-x_green">Perfil</h1>
    </div>
</div>
