@props([
    'nivel',
    'duracion',
    'proximo' => true
])
<div class="fixed py-24 top-0 left-0 w-full h-full bg-black flex items-center justify-center z-10"
    style="background-color: rgba(24,102,8, 0.6);">
    <div class="px-6 py-8 bg-white relative z-50 max-w-sm sm:max-w-lg md:max-w-xl lg:max-w-2xl shadow-lg">
        <div class="px-4">
            <div class="mt-8">
                <h1 class="text-2xl md:text-5xl leading-none text-x_green font-industry text-center py-3 bg-x_green_lightest">Felicidades</h1>
                <p class="text-x_green text-2xl md:text-4xl font-industry text-center leading-tight mt-6">
                    Completaste el nivel {{ $nivel }}  de la trivia
                </p>
            </div>
            <p class="text-x_green_light font-industry text-xl md:text-3xl mt-10 text-center">
                ¿Deseas dejar el <br> <img class="inline-block align-text-bottom" src="{{ asset('images/logo_xbox_black.png') }}" alt=""> {{ $duracion }} <br> y avanzar al siguiente nivel?
            </p>
            <div class="mt-8 flex flex-col md:flex-row items-center justify-evenly">
                <x-input.link_button wire:click="reclamar_premio" class="cursor-pointer w-full md:w-5/12 block py-6 border-x_green_light border-2 text-x_green_light uppercase font-amsi mt-4 md:mt-0 text-center md:text-lg hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" texto="Reclamar premio"/>
                @if($proximo)
                <x-input.link_button wire:click="pasarNivel" class="cursor-pointer w-full md:w-6/12 block py-6 text-x_green bg-x_green_lightest uppercase font-amsi mt-4 md:mt-0 text-center md:text-lg hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" texto="Pasar al siguiente nivel"/>
                @endif
            </div>
        </div>
    </div>
</div>
