@props([
	'estados' => []
])
<div class="fixed top-0 left-0 w-full h-full bg-black flex items-center justify-center z-10" style="background-color: rgba(24,102,8, 0.6);">
	<div class="px-6 py-8 bg-white relative z-50 w-2/5">
		<div class="w-6 h-6 p-1 ml-auto cursor-pointer" x-on:click="open=false">
			<img class="w-full h-auto" src="{{ asset('images/cerrar.png') }}" alt="">
		</div>
		<div class="px-16">
			<div class="mt-8">
		        <h1 class="text-5xl leading-none text-x_green font-industry text-center py-3 bg-x_green_lightest">Registra</h1>
		        <p class="text-x_green text-5xl font-industry text-center leading-tight">Tu ticket para poder participar</p>
		    </div>
			<div class="mt-8">
				<form>
					<div class="px-20">
						<x-input.text_alt wire:model.lazy="ticket" id="ticket" name="ticket" placeholder="Número de ticket" :error="$errors->first('nombre')" autofocus/>

						<select wire:model="estado" id="estado" class="appearance-none text-x_green_light text-2xl w-full outline-none border-b-2 border-x_green_light">
							<option value="">Selecciona un estado</option>
							@foreach($estados as $id => $nombre)
							<option wire:click="mostrarCiudades" value="{{ $id }}">{{ $nombre }}</option>
							@endforeach
						</select>
					</div>
					<p class="text-x_green_light font-arial text-2xl normal-case mt-10">
						<span class="uppercase font-bold">Atención:</span> Por favor toma una fotografía de cada uno de los tickets que registres, ya que si resultas
						ganador será necesaria para reclamar tu premio.
					</p>
					<div class="flex items-center justify-between mt-8">
                    	<x-input.button x-on:click="open=false" type="button" class="btn btn-xs btn-ghost_green uppercase font-amsi" texto="Cancelar"/>
						<x-input.button type="submit" class="btn btn-xs btn-alt uppercase font-amsi text-x_green" texto="regístrate aquí"/>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
