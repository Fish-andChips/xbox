@props([
    'error' => false,
    'label' => false
])

<div class="py-3">
    <div
        x-data
        x-init="new Pikaday({ field: $refs.input, format: 'DD/MM/YYYY', theme: 'xbox-theme', yearRange: [1930, 2020], keyboardInput: false, i18n: {
            previousMonth : 'Mes anterior',
            nextMonth     : 'Mes siguiente',
            months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            weekdays      : ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
            weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
        } })"
        @change="$dispatch('input', $event-target.value)"
    >
        <input
            x-ref="input"
           {{ $attributes }}
           class="text-lg lg:text-xl xl:text-2xl text-white font-arial appearance-none bg-transparent block w-full py-2 border-b-2
           border-white placeholder-white focus:outline-none transition duration-150 ease-in-out sm:leading-5
           @if($error) border-red-300 @endif"
           type="text"
           autocomplete="off"
           onchange="this.dispatchEvent(new InputEvent('input'))" />

        @if(!$error)
           <label class="block mt-1 text-xs text-green-200 font-bold normal-case">{{ ($label) ? $label : $attributes['placeholder'] }}</label>
        @endif
    </div>

    <div>
        @if($error)
            <p class="error_msg mt-2 text-xs text-red-400 normal-case font-semibold">{{ $error }}</p>
        @endif
    </div>

</div>
