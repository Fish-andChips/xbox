@props([
    'error' => false
])

<div class="py-3">

    <div>
        <input
            {{ $attributes }}
            type="text"
            class="textlg md:text-2xl text-x_green_light font-arial appearance-none bg-transparent
            block w-full py-2 border-b-2 border-x_green_light placeholder-x_green_light
            focus:outline-none transition duration-150 ease-in-out sm:leading-none
            @if($error) border-red-300 @endif" />
    </div>

    @if($error)
        <p class="error_msg mt-2 text-xs text-red-400 normal-case font-semibold">{{ $error }}</p>
    @endif
</div>
