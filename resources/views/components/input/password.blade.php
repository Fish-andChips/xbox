@props([
    'error' => false,
    'label' => false
])

<div class="py-3">

    <div>
        <input
            {{ $attributes }}
            type="password"
            class="text-lg lg:text-xl xl:text-2xl text-white font-arial appearance-none bg-transparent block w-full py-2
            border-b-2 border-white placeholder-white focus:outline-none transition duration-150 ease-in-out
            sm:leading-none
            @if($error) border-red-300 @endif" />

        @if(!$error)
            <label class="block mt-1 text-xs text-green-200 font-bold normal-case">{{ ($label) ? $label : $attributes['placeholder'] }}</label>
         @endif
    </div>

    <div>
        @if($error)
            <p class="error_msg mt-2 text-xs text-red-400 normal-case font-semibold">{{ $error }}</p>
        @endif
    </div>

</div>
