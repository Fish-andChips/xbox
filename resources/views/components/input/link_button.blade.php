@props([
	'texto'
])
<a {{ $attributes }}>{{ $texto }}</a>
