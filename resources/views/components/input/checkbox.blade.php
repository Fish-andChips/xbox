@props([
	'error' => false,
	'texto'
])

<div class="my-3">
	<label class="flex justify-start items-start block">
        <div class="bg-white text-white border-2 border-x_green_light w-6
                    h-6 flex flex-shrink-0 justify-center items-center mr-2
                    @if($error) bg-red-100 border-red-500 @endif">
			<input type="checkbox" class="opacity-0 absolute" {{ $attributes }}>
			<svg class="fill-current hidden w-4 h-4 text-green-500 pointer-events-none" viewBox="0 0 20 20"><path d="M0 11l2-2 5 5L18 3l2 2L7 18z"/></svg>
		</div>
		<div class="select-none normal-case ml-2 text-white">{{ $texto }}</div>
    </label>

	<div class="ml-10">
		@if($error)
			<p class="mt-2 text-xs text-red-400 normal-case font-semibold">{{ $error }}</p>
		@endif
	</div>
</div>
