@props([
    'fish' => false
])
<div class="mt-12 mx-auto w-11/12 max-w-xs sm:max-w-lg md:max-w-xl text-center font-industry relative">
    @if($fish)
    <img class="hidden md:block absolute top-0 right-0 -mt-24 -mr-24 w-10 md:w-12" src="{{ asset('images/x_titulo.svg') }}" alt="">
    <img class="absolute -mt-5 -ml-5 md:mt-20 md:-ml-24 z-0 w-10 md:w-12" src="{{ asset('images/x_titulo.svg') }}" alt="">
    @endif
    <div class="py-3 relative">
        <h1 class="text-2xl sm:text-4xl md:text-5xl leading-none text-x_green_lightest">Compra un blizzard,</h1>
    </div>
    <p class="text-lg sm:text-2xl md:text-3xl leading-none mt-2 text-white">responde las trivas y gana un</p>
    <p class="text-lg sm:text-3xl leading-snug text-white"><span class="text-x_green_light">game pass</span> o hasta un <span class="text-x_green_light">xbox one x</span></p>
    <p class="text-lg sm:text-3xl leading-snug text-white">registrando tu ticket de compra.</p>
</div>

