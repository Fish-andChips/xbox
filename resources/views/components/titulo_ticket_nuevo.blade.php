<div class="mt-12 mx-auto w-11/12 sm:max-w-lg md:max-w-xl text-center font-industry">
    <div class="py-3 bg-x_green_lightest max-w-sm mx-auto">
        <h1 class="text-2xl sm:text-4xl md:text-5xl leading-none text-x_green">Tu ticket</h1>
    </div>
    <p class="text-2xl md:text-3xl lg:text-5xl leading-none mt-2 text-white">se ha registrado con éxito</p>
    <p class="mt-5 md:mt-16 text-lg md:text-2xl leading-snug text-white font-arial normal-case">Ahora responde las trivias para ganar</p>
</div>
