<div>
    <x-titulo_faqs />
    <!--a href="{{ url()->previous() }}">Atrás</a-->
	<div class="mt-24 mx-auto max-w-xs sm:max-w-lg md:max-w-xl lg:max-w-3xl xl:max-w-5xl text-white">
		@foreach($faqs as $q => $a)
		    <div x-data="{open:false}" class="mt-2 py-2">
		    	<h3 x-on:click="open = !open" class="text-lg sm:text-xl md:text-3xl lg:text-4xl hover:text-green-200 font-industry cursor-pointer leading-tight">{{$q}}</h3>
                <p
                    x-show="open"
		    		class="font-arial text-sm md:text-lg lg:text-xl leading-tight normal-case mt-4 border-b border-green-100 pb-2"
					x-transition:enter="transition ease-out duration-300"
					x-transition:enter-start="opacity-0 transform scale-90"
					x-transition:enter-end="opacity-100 transform scale-100"
		    	>
		    		{{ $a }}
		    	</p>
		    </div>
	    @endforeach
    </div>
</div>
