<div class="mx-auto max-w-xs sm:max-w-lg md:max-w-2xl mt-16 flex flex-col items-center justify-center">
    <div class="text-white flex flex-col items-center justify-center w-full text-2xl sm:text-4xl md:text-5xl py-12 border-b-2 border-t-2 border-dashed border-x_green_lightest">
        <img class="mt-8 w-10 md:w-12" src="{{ asset('images/game_over.svg') }}" alt="">
        <span class="font-industry text-4xl md:text-6xl lg:text-7xl text-x_green_lightest leading-tight">Game</span>
        <span class="font-industry text-4xl md:text-6xl lg:text-7xl text-x_green_lightest leading-tight">Over</span>
    </div>

    <p class="text-white font-industry text-center leading-none mt-10 text-xl sm:text-3xl lg:text-5xl">
        Ingresa otro ticket <br>  para intentarlo de <br> nuevo
    </p>
    <div class="mt-20 w-full max-w-xs md:max-w-sm px-10">
        <x-input.link_button href="{{ route('tickets') }}" class="mx-auto block py-6 text-white font-amsi text-center border-2 border-white" texto="Regresar al home"/>
    </div>

    <script>
        window.clearInterval(window.timer);
    </script>
</div>
