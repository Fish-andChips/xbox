<div x-data>
	@if($nivel == 1)
		<x-titulo_ticket_nuevo />
    @else
        <x-titulo_ticket_nivel />
    @endif

    <div>
		<div class="mt-24 px-4 mx-auto w-full md:max-w-3xl lg:max-w-5xl flex flex-col md:flex-row items-center justify-between">
            <div class="trivia_img shadow-2xl p-5 {{ ($nivel > 1) ? 'passed pointer-events-none' : ''}}">
                <a class="relative" href="{{ route('responder_trivia') }}">
                    @if($nivel == 1)
                        <img src="{{ asset('images/xbox_control.png') }}" alt="trivia imagen" class="mr-auto">
                    @elseif($nivel > 1)
                        <img src="{{ asset('images/badge.png') }}" alt="trivia imagen" class="mr-auto">
                    @else
                        <img src="{{ asset('images/candado.png') }}" alt="trivia imagen" class="ml-auto">
                    @endif
                    <div class="mt-8 font-industry">
                        <h4 class="text-5xl {{ ($nivel > 1)?  'text-white':'text-x_green_light'}}">Nivel 1</h4>
                        <span class="text-2xl text-white">Básico</span>
                    </div>
                </a>
	        </div>
            <div class="p-5 my-10 md:my-0 trivia_img shadow-2xl  @if($nivel > 2) passed pointer-events-none @elseif($nivel < 2) disable pointer-events-none @endif">
                <a class="relative" href="{{ route('responder_trivia') }}">
                    @if($nivel == 2)
                        <img src="{{ asset('images/xbox_control.png') }}" alt="trivia imagen" class="mr-auto">
                    @elseif($nivel > 2)
                        <img src="{{ asset('images/badge.png') }}" alt="trivia imagen" class="mr-auto">
                    @else
                        <img src="{{ asset('images/candado.png') }}" alt="trivia imagen" class="ml-auto">
                    @endif
                    <div class="mt-8 font-industry">
                        <h4 class="text-5xl {{ ($nivel > 2)?  'text-white':'text-x_green_light'}}">Nivel 2</h4>
                        <span class="text-2xl text-white">Medio</span>
                    </div>
                </a>
	        </div>
            <div class="trivia_img shadow-2xl p-5 @if($nivel > 3) passed pointer-events-none @elseif($nivel < 3) disable pointer-events-none @endif">
                <a class="relative" href="{{ route('responder_trivia') }}">
                    @if($nivel == 3)
                        <img src="{{ asset('images/xbox_control.png') }}" alt="trivia imagen" class="mr-auto">
                    @elseif($nivel > 3)
                        <img src="{{ asset('images/badge.png') }}" alt="trivia imagen" class="mr-auto">
                    @else
                        <img src="{{ asset('images/candado.png') }}" alt="trivia imagen" class="ml-auto">
                    @endif
                    <div class="mt-8 font-industry">
                        <h4 class="text-5xl {{ ($nivel > 3)?  'text-white':'text-x_green_light'}}">Nivel 3</h4>
                        <span class="text-2xl text-white">Pro</span>
                    </div>
                </a>
	        </div>
        </div>

        @if($nivel == 4)

            <!--div data-animation='first'>
                <div></div>
                <div></div>
            </div-->
            <x-modal.ultimo_nivel />

            <div id="nivel4" class="py-16 border-white border-t-2 mt-24 px-4 mx-auto w-full max-w-xs md:max-w-3xl lg:max-w-5xl flex flex-col items-center justify-between overflow-hidden">
                <p class="text-2xl md:text-3xl lg:text-5xl leading-none mt-2 text-white font-industry text-center">Llegaste a la ronda final</p>
                <div class="mt-16 trivia_img shadow-xl master p-5">
                    <a class="relative" href="{{ route('responder_trivia') }}">
                        <div class="mt-8 font-industry flex flex-col items-center">
                            <img src="{{ asset('images/xbox_icon.png') }}" alt="trivia imagen">
                            <h4 class="text-4xl md:text-5xl text-x_green_light">Último nivel</h4>
                            <span class="text-2xl text-white">Master</span>
                        </div>
                    </a>
                </div>
            </div>
        @endif
    </div>

    @livewire('trivias.concursantes');

    @livewire('contador-premios')

    <!-- Modal no permitir participar -->
    @if($no_ticket)

    <div>
        <div class="fixed py-24 top-0 left-0 w-full h-full bg-black flex items-center justify-center z-10" style="background-color: rgba(24,102,8, 0.6);">
            <div class="px-6 py-8 bg-white relative z-50 max-w-sm sm:max-w-lg md:max-w-xl lg:max-w-2xl shadow-lg">
                <div class="px-16">
                    <div class="mt-8">
                        <h1 class="text-2xl md:text-5xl leading-none text-x_green font-industry text-center py-3 bg-x_green_lightest">Atencion</h1>
                        <p class="text-x_green text-2xl md:text-5xl font-industry text-center leading-tight mt-6">No tienes ningún ticket activo</p>
                    </div>
                    <p class="text-x_green_light font-arial text-lg md:text-2xl normal-case mt-10 text-center">
                        Dirígete a la sección de tickets y registra uno nuevo para seguir participando
                    </p>
                    <div class="mt-8 flex flex-col md:flex-row items-center justify-evenly">
                        <x-input.link_button href="{{ route('tickets') }}"
                        class="cursor-pointer w-full md:w-5/12 block py-6 border-x_green_light border-2 text-x_green_light
                        uppercase font-amsi mt-4 md:mt-0 text-center md:text-lg hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" texto="Registrar ticket"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endif
</div>
