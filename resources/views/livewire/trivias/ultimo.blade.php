<div class="mt-24 px-4 mx-auto w-full md:max-w-3xl lg:max-w-5xl">
    <div>
        <img src="{{ asset('images/xbox_control.png') }}" alt="">
        <div class="pt-4 pb-20 border-dashed border-b-2 border-t-2 border-x_green_lightest mt-6">
            <p class="font-industry text-x_green_lightest md:text-3xl">Llegaste a la parte final del reto, es <br class="hidden md:block"> momento de arriesgarlo todo por el gran <br class="hidden md:block"> premio</p>
            <div class="mt-4">
                <p class="font-arial text-white md:text-2xl normal-case">Estas a una respuesta de ganarlo todo o perderlo todo, aquí tu <br class="hidden md:block"> conocimiento gamer tiene que salir a relucir. </p>
            </div>
        </div>
        @if(!$comenzar)
        <div class="mt-8">
            <a wire:click="comenzarTrivia" class="block w-56 mx-auto py-4 bg-x_green_light text-white text-center font-industry md:text-2xl cursor-pointer hover:bg-x_green_lightest">Empezar</a>
        </div>
        @endif
    </div>

    @if($comenzar)
        @livewire('trivias.ultima-trivia', ['trivia' => $trivia_actual, 'numero_pregunta' => $pregunta_actual, 'ultima' => $ultima]);
    @endif

    @if($mostrar_ganador)
        <x-modal.ganador_xbox />

        <script>
            window.clearInterval(window.timer);
        </script>
    @endif

</div>
