<div
    x-data="{ tiempo_total: {{ $tiempo_limite }}, tiempo_transcurrido: 0 }"
    x-init="window.timer = setInterval(function(){
        if(tiempo_transcurrido < tiempo_total) {
            tiempo_transcurrido++;
            $refs.barra_progreso.style.width = ((tiempo_transcurrido * 100) / tiempo_total) + '%';
        }
        else
        {
            $refs.time_over.click();
        }
    }, 1000)"
>
    <div class="mx-auto w-full max-w-sm sm:max-w-md md:max-w-lg lg:max-w-2xl">
        @if($respuesta_incorrecta || $tiempo_agotado)
            <div class="text-white mt-8">
                <p class="text-center font-industry text-xl md:text-2xl">
                    {{ ($respuesta_incorrecta) ? 'respuesta incorrecta' : 'se acabo el tiempo' }}<br>¡game over!
                </p>

                <img class="mx-auto mt-8" src="{{ asset('images/craneo.png') }}" alt="">
            </div>
        @else
            <div class="flex items-center px-12 mt-8">
                <input x-ref="time_over" type="hidden" wire:click="timeOver">
                <div id="i_bomb" class="w-10 md:w-16 text-x_green_lightest">
                    <svg xmlns="http://www.w3.org/2000/svg" class="fill-current" viewBox="0 0 512 512">
                        <defs/>
                        <path d="M353.982 77.822l-33.953 33.953-21.078-21.078c-17.006-17.006-46.682-16.977-63.629 0l-6.548 6.533c-73.912-19.804-153.734 8.817-197.838 73.106-40.354 58.81-41.307 138.273-2.402 197.757 64.464 98.479 201.098 110.099 281.023 30.173 45.759-45.759 63.16-111.599 46.491-173.764l6.533-6.533c17.548-17.548 17.548-46.096 0-63.643l-21.342-21.342 33.953-33.953c17.222-17.222 46.813-4.876 46.813 19.394v197.566c0 8.291 6.709 14.999 14.999 14.999 8.29 0 14.999-6.709 14.999-14.999V118.424c0-50.822-61.946-76.678-98.021-40.602zM103.838 318.819c4.541 6.929 2.593 16.229-4.351 20.756-6.839 4.493-16.197 2.651-20.756-4.351-25.34-38.729-24.637-92.588 1.685-130.949 4.702-6.841 14.032-8.555 20.844-3.882 6.841 4.688 8.569 14.018 3.882 20.844-19.628 28.621-20.155 68.74-1.304 97.582zm333.167 42.169c-8.291 0-14.999 6.709-14.999 14.999v29.998c0 8.291 6.709 14.999 14.999 14.999 8.29 0 14.999-6.709 14.999-14.999v-29.998c0-8.29-6.708-14.999-14.999-14.999zm70.601 4.395l-21.21-21.21c-5.859-5.859-15.351-5.859-21.21 0s-5.859 15.351 0 21.21l21.21 21.21c5.859 5.859 15.351 5.859 21.21 0s5.859-15.352 0-21.21z" class="active-path" data-old_color="#000000" data-original="#000000"/>
                        <path d="M408.823 344.173c-5.859-5.859-15.35-5.86-21.21 0l-21.21 21.21c-5.859 5.859-5.859 15.351 0 21.21s15.351 5.859 21.21 0l21.21-21.21c5.859-5.859 5.859-15.351 0-21.21z" class="active-path" data-old_color="#000000" data-original="#000000"/>
                    </svg>
                </div>

                <div id="barra_tiempo" class="mx-4 flex-grow h-2 bg-white rounded-full overflow-hidden">
                    <div
                        x-ref="barra_progreso"
                        class="h-full text-right bg-x_green_lightest w-0 transition-all duration-700 ease-in">
                    </div>
                </div>

                <div id="i_explosion" class="w-10 md:w-16 text-white">
                    <svg xmlns="http://www.w3.org/2000/svg" class="fill-current" viewBox="0 0 512.786 512.786">
                        <defs/>
                        <path d="M478.354 75.476C457 57.838 428.78 48.125 398.894 48.125c-1.546 0-3.033.24-4.434.679-1.186.371-2.464-.041-3.292-.968-13.3-14.892-32.631-24.288-54.122-24.288a72.454 72.454 0 00-23.425 3.88 3.147 3.147 0 01-3.509-1.075C297.623 9.96 278.081 0 256.758 0c-23.571 0-44.81 12.14-56.867 31.417a3.183 3.183 0 01-4.239 1.109c-10.603-5.854-22.565-8.978-34.991-8.978-21.271 0-40.421 9.211-53.707 23.839a3.186 3.186 0 01-2.152 1.044c-26.482 1.763-51.194 11.207-70.37 27.045C12.247 93.8.028 118.413.028 144.781c0 24.019 10.405 47.033 29.298 64.804 18.212 17.13 43.136 28.168 70.18 31.08.546.059 1.087.087 1.624.087h.012c.751-.001 1.48.23 2.07.696 12.278 9.683 27.703 15.296 44.193 15.296a70.835 70.835 0 0028.686-6.079c1.247-.552 2.698-.199 3.621.804 19.67 21.381 47.723 33.874 77.048 33.874 30.07 0 57.561-12.798 76.818-33.629.918-.993 2.353-1.348 3.596-.814 8.878 3.814 18.513 5.844 28.211 5.844 16.63 0 31.944-5.725 44.09-15.299a3.264 3.264 0 012.064-.693l.121.001c.536 0 1.079-.028 1.624-.087 27.044-2.912 51.967-13.95 70.18-31.08 18.893-17.771 29.298-40.786 29.298-64.804-.004-26.369-12.222-50.982-34.408-69.306zM425.089 331.666c-16.221-5.299-37.055-9.712-61.198-13.023-.049 2.162-.09 4.324-.09 6.477 0 8.058.348 16.051.997 23.917 44.707 6.514 65.048 15.531 71.674 20.487-.428-.282-3.974 2.307-4.459 2.592-4.334 2.545-9.065 4.31-13.803 5.942-12.15 4.185-24.786 7.023-37.415 9.318-2.987.543-5.985 1.11-8.99 1.54-9.646 1.561-19.902 2.917-30.647 4.05 2.291 10.213 5.094 20.008 8.349 29.275 26.568-3.015 53.567-7.008 78.915-15.845 5.584-1.947 11.036-4.234 16.297-6.937 15.624-8.03 23.546-18.099 23.546-29.926-.001-23.764-32.498-34.379-43.176-37.867zM335.762 289.673c-22.816 16.517-50.511 25.671-79.004 25.671-28.366 0-55.93-9.077-78.698-25.462a323.106 323.106 0 011.937 35.238c0 23.558-2.583 46.54-7.339 67.768 26.073 2.776 54.976 4.252 84.605 4.252 29.359 0 58.008-1.448 83.894-4.176-4.767-21.25-7.357-44.258-7.357-67.845a322.872 322.872 0 011.962-35.446zM124.926 486.602a15 15 0 009.996 26.184h243.952a15 15 0 009.996-26.184c-16.322-14.59-29.815-37.163-39.365-64.363-28.211 3.201-59.657 4.9-92.242 4.9-32.855 0-64.553-1.727-92.944-4.98-9.552 27.237-23.055 49.84-39.393 64.443z" class="active-path" data-old_color="#000000" data-original="#000000"/>
                        <path d="M94.482 377.412c-8.582-3.085-13.718-5.89-16.427-7.887 6.591-4.931 26.743-13.876 70.935-20.38a291.016 291.016 0 001.006-24.024c0-2.12-.041-4.249-.088-6.378-23.765 3.29-44.292 7.653-60.333 12.879-10.712 3.491-43.311 14.112-43.312 37.91 0 14.123 12.019 24.001 23.546 29.926 11.032 5.67 26.399 10.67 45.675 14.86 16.113 3.504 32.458 5.966 48.835 7.842 3.25-9.267 6.05-19.061 8.338-29.272-10.668-1.136-20.852-2.493-30.428-4.053-16.08-2.619-32.384-5.902-47.747-11.423z" class="active-path" data-old_color="#000000" data-original="#000000"/>
                    </svg>
                </div>
            </div>
        @endif
    </div>

    <div class="mt-20 mx-auto max-w-2xl lg:max-w-screen-lg text-center">
        <p class="text-white text-2xl sm:text-3xl md:text-4xl lg:text-5xl font-industry normal-case leading-none">{{ $pregunta }}</p>
        <div class="flex flex-wrap items-center justify-center mt-10 px-2 font-industry">

            @foreach($respuestas as $opcion => $texto)
                <div class="w-full lg:w-1/3 @if($loop->iteration == 2) my-5 md:my-0 @endif">
                    <div
                        wire:click="responderTrivia({{$opcion}})"
                        class="@if($respuesta_incorrecta || $tiempo_agotado) cursor-default pointer-events-none @else cursor-pointer @endif">
                        <p class="flex flex-row items-center justify-start text-outline leading-none">
                            @if(($respuesta_incorrecta || $tiempo_agotado) && ($loop->iteration == $respuesta))
                                <span class="text-white text-5xl md:text-7xl mr-5 leading-none">{{ $opcion }}</span>
                                <span class="text-white text-3xl md:text-4xl leading-none">{{ $texto }}</span>
                            @else
                                <span class="text-black text-5xl md:text-7xl mr-5 leading-none">{{ $opcion }}</span>
                                <span class="text-black text-3xl md:text-4xl leading-none">{{ $texto }}</span>
                            @endif
                        </p>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>

