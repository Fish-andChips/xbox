<div x-data>
    <div class="mt-16 max-w-2xl mx-auto">
        <div class="flex flex-col items-center justify-center">
            <img src="{{ asset('images/xbox_control.png') }}" alt="">
            <h4 class="text-x_green_lightest text-2xl md:text-5xl font-industry">Nivel {{ $nivel }}</h4>
            <h6 class="text-white font-industry text-lg md:text-2xl">{{ $nombre_nivel }}</h6>
            <p class="text-x_green_lightest font-industry text-lg md:text-2xl">{{ $pregunta_actual }} de {{ $numero_preguntas }}</p>
        </div>
    </div>

    @livewire('trivias.pregunta', ['trivia' => $trivia_actual, 'ultima' => $ultima]);

    @livewire('trivias.concursantes');

    @livewire('contador-premios')

    @if($ultima)
    <script>
        clearInterval(window.timer);
    </script>
    @endif

    <!-- mostrar perderdor -->
    @if($mostrar_siguiente_nivel)
        @if($nivel == 1 || $nivel == 2 || $nivel == 3)
            <x-modal.gamepass nivel={{$nombre_nivel}} duracion={{$duracion_gamepass}} proximo={{$mostrar_boton_siguiente}} />
        @else
            <x-modal.xbox reclamado={{$reclamado}}/>
        @endif
        <script>
            window.clearInterval(window.timer);
        </script>
    @endif

    <!-- modal reclamar premio -->
    @if($reclamar_premio)
        <x-modal.reclamar_premio nivel={{$nivel}} reclamado={{$reclamado}} email={{$email}} ticket={{$codigo_ticket}} />
    @endif

    @if($mostrar_no_premios)
        <x-modal.no_premios />
    @endif


</div>
