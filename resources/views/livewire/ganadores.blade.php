<div>
    <div class="mt-12 mx-auto w-11/12 max-w-xs sm:max-w-lg md:max-w-xl text-center font-industry">
        <div class="py-3 bg-x_green_lightest">
            <h1 class="text-2xl sm:text-4xl md:text-5xl leading-none text-x_green">Lista de Ganadores</h1>
        </div>
    </div>
    <div class="mt-12 mx-auto w-11/12 max-w-xs sm:max-w-sm text-center">
        <img src="{{ asset('images/ganador_xbox_2.jpg') }}" alt="">
        <p class="text-white mt-4 normal-case font-arial">Ganador Xbox One X - Semana 3</p>
    </div>
    <div class="mt-16 mx-auto max-w-xs sm:max-w-lg lg:max-w-3xl xl:max-w-4xl text-white">
        <table class="table-auto mx-auto text-xs md:text-base">
            <thead>
              <tr>
                <th class="px-4 py-2">Semana</th>
                <th class="px-4 py-2">Nombre</th>
                <th class="px-4 py-2">Ciudad</th>
              </tr>
            </thead>
            <tbody>

            @foreach($ganadores as $semana => $lista)
              @foreach($lista as $ganador)
              <tr>
                <td class="border px-4 py-2">{{$semana}}</td>
                <td class="border px-4 py-2">{{ $ganador['nombre'] }}</td>
                <td class="border px-4 py-2">{{$ganador['ciudad']}}</td>
              </tr>
              @endforeach
            @endforeach
            </tbody>
        </table>
    </div>
</div>
