<div>
    <x-titulo_terminos />
    <div class="mt-16 mx-auto max-w-xs sm:max-w-lg lg:max-w-3xl xl:max-w-4xl text-white">
    	<div class="normal-case font-arial text-sm md:text-base leading-normal">
            <div class="mt-4 pl-4">
                Los términos que a continuación se presentan regulan y constituyen el acuerdo de los SERVICIOS e INFORMACIÓN proporcionados por la sociedad JUST BDP SOLUTIONS S.A. DE C.V., que en lo sucesivo se denominará BDP, en representación de DAIRY QUEEN y cualquier individuo que accede o navega por, o se registra como usuario de la página Web o sitio de BDP que en lo sucesivo se denominará USUARIO.
                <br><br><span class="font-bold -ml-4">1. TÉRMINOS DE USO.</span><br/>
                Los términos que a continuación se presentan, regulan el uso del portal DQ + XBOX, el cual ha sido puesto a disposición por JUST BDP SOLUTIONS S.A. DE C.V. en representación de DAIRY QUEEN, a través de la siguiente dirección electrónica: www.datelavueltaconxbox.com
                La utilización del portal DQ + XBOX por parte de cualquier persona, le atribuye la calidad de USUARIO lo cual implica su adhesión plena e incondicional a los presentes TÉRMINOS Y CONDICIONES; en consecuencia es indispensable que el USUARIO los lea y evalúe de forma cuidadosa ANTES de acceder al portal DQ + XBOX,  de tal manera que esté consciente de que se sujeta a los mismos y a las modificaciones que pudiera sufrir, cada vez que accede al portal DQ + XBOX.
                Si en algún momento el USUARIO no estuviera de acuerdo total o parcialmente con los términos de los presentes TÉRMINOS Y CONDICIONES, deberá abstenerse inmediatamente de usar el portal DQ + XBOX, en cualquiera de sus partes o secciones.
                El USUARIO está de acuerdo en que en diversas partes del portal DQ + XBOX, podrá haber estipulaciones especiales (en lo sucesivo CONDICIONES PARTICULARES) que, según sea el caso, sustituirán, complementarán y/o modificarán los presentes TÉRMINOS Y CONDICIONES, por tal razón también resulta indispensable que el USUARIO las lea previamente en cada caso y de no estar total o parcialmente de acuerdo con ella o ellas, se abstenga de usar la parte o sección del portal DQ + XBOX, regulada bajo esas CONDICIONES PARTICULARES. En caso de que el USUARIO utilice parte del portal regulada por una o más CONDICIONES PARTICULARES, se entenderá que se somete total e incondicionalmente a ellas.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">2. OBJETO</span><br/>
                Se entenderá por CONTENIDO el conjunto de DATOS, INFORMACIÓN, SERVICIOS y/o OPCIONES ADICIONALES proporcionadas en el portal DQ + XBOX.
                El CONTENIDO en el portal DQ + XBOX es propiedad de BDP, quien es titular de todos los derechos de autor, de propiedad industrial y derechos sobre signos distintivos y avisos comerciales, sean derivados e inherentes de los mismos, salvo que exista un señalamiento particular en contrario dentro del mismo. Por lo anterior, BDP se reserva el derecho a modificar en cualquier momento y sin previo aviso la presentación, configuración, información, OPCIONES ADICIONALES, DATOS y en general cualquier parte o aspecto relacionado directa o indirectamente con el CONTENIDO del portal DQ + XBOX.
                Las modificaciones relacionadas con el presente documento, surtirán efectos desde el momento de su publicación en el portal DQ + XBOX.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">3. ACCESO Y UTILIZACIÓN DEL PORTAL DQ + XBOX</span><br>
                El Acceso y Utilización del portal DQ + XBOX son de carácter gratuito para el USUARIO, salvo los servicios que expresamente lo indiquen.
                El usuario estará obligado a registrarse o inscribirse en el portal DQ + XBOX únicamente cuando se le solicite específicamente.
                El USUARIO se obliga a verificar que al salir del portal DQ + XBOX no se encuentre su cuenta de correo electrónico, contraseña ni su información personal ingresada o grabada en el portal de inicio del mismo para evitar que un tercero haga uso de las mismas.
                Por tanto, es responsabilidad del USUARIO proteger su contraseña para que no se haga mal uso de la misma.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">4. OBLIGACIÓN DE HACER USO CORRECTO Y LÍCITO DEL PORTAL DQ + XBOX, LOS DATOS, LAS OPCIONES, LAS OPCIONES ADICIONALES, Y EN GENERAL DE SU CONTENIDO, CONFORME A LA LEGISLACIÓN APLICABLE DEL PRESENTE AVISO Y LAS BUENAS COSTUMBRES.</span><br/>
                El USUARIO se obliga a utilizar el portal DQ + XBOX, los DATOS, OPCIONES y OPCIONES ADICIONALES conforme a las leyes aplicables, a lo dispuesto en estos TÉRMINOS Y CONDICIONES, en respeto al orden público, la moral y las buenas costumbres. El USUARIO se obliga utilizar el portal DQ + XBOX, los DATOS, OPCIONES y OPCIONES ADICIONALES, de una manera en que no lesione derechos o intereses de BDP o de las personas vinculadas a él directa o indirectamente, o de terceros.
                El USUARIO utilizará el portal DQ + XBOX, los DATOS, OPCIONES y/o OPCIONES ADICIONALES de una manera en la que no dañe, inutilice, deteriore o menoscabe total o parcialmente el CONTENIDO.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">5. MEDIOS PARA OBTENCIÓN DE CONTENIDO.</span><br/>
                El USUARIO deberá abstenerse de obtener o intentar obtener, utilizar o intentar utilizar, reproducir o intentar reproducir información, mensajes, gráficos, dibujos, archivos de sonido y/o imagen, fotografías, grabaciones, software y demás material del CONTENIDO presentado a través del portal DQ + XBOX, salvo que expresamente lo autorice BDP y únicamente mediante los procedimientos establecidos dentro del portal DQ + XBOX.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">6. USO CORRECTO DEL CONTENIDO.</span><br/>
                El USUARIO se obliga a usar el CONTENIDO de forma diligente, correcta y lícita, y en particular, se compromete a abstenerse de: (a) utilizarlo de forma, con fines o efectos contrarios a la ley, a la moral, al orden público o a lo establecido por BDP para el uso del portal DQ + XBOX, a través de los presentes TÉRMINOS Y CONDICIONES; (b) copiar, modificar, reproducir, distribuir o utilizar de cualquier forma con o sin fines de lucro el CONTENIDO, a menos que cuente con la autorización expresa y por escrito de BDP; (c) modificar, manipular o utilizar con o sin fines de lucro, las marcas, logotipos, avisos comerciales, nombres comerciales y signos distintivos en general propiedad de BDP, o de los cuales sea licenciatario, contenidos en el portal DQ + XBOX o de los PRESTADORES DE OPCIONES ADICIONALES (salvo que cuente con su autorización por escrito); (d) suprimir, eludir o modificar el CONTENIDO, los dispositivos técnicos de protección o seguridad, o cualquier mecanismo o procedimiento establecido en el portal DQ + XBOX; (e) suprimir, eludir o manipular o utilizar con o sin fines de lucro la información relacionada con Derechos de Autor y demás datos contenidos en el portal DQ + XBOX, y/o de los PRESTADORES DE OPCIONES ADICIONALES sean titulares o licenciatarios.
                BDP manifiesta que el CONTENIDO utilizado en el portal DQ + XBOX se encuentra debidamente registrado y protegido ante las autoridades competentes y bajo las leyes correspondientes, salvo indicación en contrario. El USUARIO se obliga a respetar y no infringir todos los derechos de Propiedad Intelectual de BDP con relación al CONTENIDO del portal DQ + XBOX.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">7. INSTRUCCIONES.</span><br/>
                El USUARIO se compromete a cumplir todas las instrucciones proporcionadas por BDP en relación con cualquier mecanismo o procedimiento establecido para utilizar el portal DQ + XBOX o el CONTENIDO.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">8. RESPONSABILIDAD POR DAÑOS Y PERJUICIOS.</span><br/>
                El USUARIO es responsable del cumplimiento de los presentes TÉRMINOS Y CONDICIONES o cualquier normatividad aplicable, por lo que el USUARIO deslinda a BDP de toda responsabilidad civil, penal, administrativa o de cualquier índole que pudiese surgir como consecuencia del uso del CONTENIDO del portal DQ + XBOX.
                Sin perjuicio de lo establecido en el párrafo anterior, en el supuesto de que BDP fuera sancionado o condenado por autoridad competente en cualquier procedimiento relacionado con responsabilidad civil, penal, administrativo o de cualquier otra índole por causas directas o indirectas imputables al USUARIO, BDP tendrá el derecho de repetir en contra del USUARIO y exigir la devolución de todas las cantidades, incluyendo multas, sanciones, costas, honorarios y demás gastos en que hubiera incurrido BDP con relación a dicho procedimiento.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">9. POLÍTICAS DE PRIVACIDAD</span><br/>
                Para utilizar o gozar de algunos de los Servicios es necesario que el Usuario proporcione previamente en el portal DQ + XBOX ciertos datos de carácter personal (en adelante, los Datos Personales) que BDP podrá administrar automáticamente o no. El usuario al acceder al Portal o a cualquiera de los servicios en que los Datos Personales son requeridos, está autorizando a BDP a: 1) Realizar análisis y estudios sobre los Datos Personales; 2) Utilizarlos para enviar información, Publicidad o Promociones al Usuario referentes a los Servicios y Productos administrados y brindados por BDP, ya sea por medios electrónicos o mediante correo postal o mensajería; 3) Utilizarlos para enviar notificaciones o avisos al Usuario sobre los servicios de BDP y 4) Utilizarlos para la consecución del Objeto de BDP y el cumplimiento cabal y eficiente de los Servicios brindados por BDP.
                El usuario se obliga a proporcionar Datos Personales verdaderos y fidedignos, y en caso que deseara no proporcionarlos en las partes del Portal que así lo requieran, deberá abstenerse de usar esa o esas partes o secciones del Portal. En caso de que el usuario diera información falsa o confusa, BDP podrá negarle el acceso al Portal y los Servicios contenidos dentro y fuera de él, sin perjuicio de que pueda requerirle las indemnizaciones a que hubiere lugar.
                Es responsabilidad del usuario mantener actualizados sus datos de registro para evitar contratiempos en los envíos de información y/o recompensas.
                Por el hecho de proporcionar sus Datos Personales en el Portal, el Usuario está autorizando a  BDP a dar a conocer a cualquier autoridad competente la información respectiva en caso de que ésta sea solicitada por los medios jurídicos adecuados. BDP  no compartirá información personal alguna, que haya sido proporcionada, con terceras personas a menos que sea requerido para proporcionar un servicio o un producto requerido por el Usuario. El usuario facilitará a BDP dicha información, para utilizar, procesar, automatizar y en general disponer de su información para los efectos de los servicios del presente portal y/o los que realice BDP  de forma física.
                La información proporcionada por el usuario está protegida por diversos mecanismos de seguridad, BDP hace todo lo posible para salvaguardar la seguridad de la información.
                Para mayor información sobre las Políticas de Privacidad para los datos personales de los Usuarios favor de visitar el apartado titulado “Aviso de Privacidad”, disponible en el URL: www.datelavueltaconxbox.com
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">10. UTILIZACIÓN DEL PORTAL DQ + XBOX, DE LAS OPCIONES Y DE LOS CONTENIDOS BAJO LA EXCLUSIVA RESPONSABILIDAD DEL USUARIO.</span><br/>
                Por el solo hecho de acceder al portal DQ + XBOX, el USUARIO reconoce y acepta que el uso de la misma, de las OPCIONES y de cualquier parte del CONTENIDO, es de su exclusiva responsabilidad, por lo que BDP no será en ningún momento y bajo ninguna circunstancia, responsable por cualquier desperfecto o problema que se presentara en el equipo de cómputo (hardware) o programas de cómputo (software) que utilice EL USUARIO para acceder o navegar en cualquier parte del portal DQ + XBOX.
                El CONTENIDO del portal DQ + XBOX incluye secciones como las OPCIONES ADICIONALES, que no necesariamente pertenecen a BDP y que constituyen una relación independiente entre el USUARIO y el PRESTADOR DE OPCIONES ADICIONALES. BDP no es responsable de la información, datos, servicios, opciones u cualquier otra información o procedimiento que el USUARIO realice o intercambie dentro de estas áreas con cualquier PRESTADOR DE OPCIONES ADICIONALES. Por lo anterior, es responsabilidad del USUARIO utilizar dichos espacios sólo para los fines y conforme a los procedimientos indicados por dicha sección.
                BDP busca asegurar la calidad y titularidad del CONTENIDO del portal DQ + XBOX; sin embargo, no es responsable de la existencia de cualquier tipo de información dentro del mismo que no cumpla con las políticas y derechos mencionados anteriormente en los presentes TÉRMINOS Y CONDICIONES.
                BDP no es, de manera alguna, responsable de la exactitud o veracidad de la información que se publica en las secciones relacionadas con el PRESTADOR DE OPCIONES ADICIONALES del portal DQ + XBOX.
                El USUARIO reconoce que toda información transmitida por él mismo en el portal DQ + XBOX, así como en las secciones de PRESTADOR DE OPCIONES ADICIONALES, es responsabilidad única del USUARIO emisor.
                El CONTENIDO del portal DQ + XBOX provee información de naturaleza general. BDP no es responsable de las acciones que el USUARIO decida tomar como resultado de la información presentada en el portal DQ + XBOX.
                BDP es ajeno a los vínculos de Internet externos y que pueden ser accedidos por medio de las OPCIONES ADICIONALES dentro del portal DQ + XBOX y por consiguiente desconoce la veracidad, capacidad y legalidad del Contenido de la información en dichos vínculos, por lo que BDP  no es responsable de manera alguna por dichos accesos o vínculos ni por los datos en ellos contenidos, incluyendo aquellos relacionados con cualquier PRESTADOR DE OPCIONES ADICIONALES.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">11. EXCLUSIÓN DE GARANTÍAS Y DE RESPONSABILIDAD POR EL FUNCIONAMIENTO DEL PORTAL DQ + XBOX CONTENIDO y OPCIONES:</span><br/>
                *Disponibilidad y continuidad
                BDP no garantiza al USUARIO la disponibilidad y continuidad del funcionamiento del portal DQ + XBOX y/o de otras OPCIONES, sin embargo, hará lo posible para que así sea.
                Por el sólo hecho de acceder al portal DQ + XBOX, el USUARIO renuncia a su derecho de ejercer cualquier acción legal en contra de BDP por cualquier vía, por la falta de disponibilidad o continuidad en el funcionamiento del portal DQ + XBOX y/o de las OPCIONES.
                *Exclusión de garantías y de responsabilidad por el CONTENIDO, virus y otros elementos dañinos.
                BDP no controla ni garantiza la ausencia de virus en el CONTENIDO, ni de otros elementos que puedan producir alteraciones en el sistema informático del USUARIO (software y/o hardware) o en los documentos electrónicos y ficheros almacenados en su sistema informático.
                Por el sólo hecho de acceder al portal DQ + XBOX, el USUARIO renuncia a su derecho de ejercer cualquier acción legal por cualquier vía en contra de  BDP, por cualquier daño o perjuicio que sufriera en su sistema informático (software y/o hardware), por haber usado el portal DQ + XBOX y/o el CONTENIDO, o porque éstos le hubieran transmitido un virus o cualquier otro elemento dañino, incluidas las secciones relacionadas con cualquier PRESTADOR DE OPCIONES ADICIONALES.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">12. LICENCIA.</span>
                BDP autoriza al USUARIO a utilizar el portal DQ + XBOX exclusivamente en los términos y condiciones establecidas en los presentes TÉRMINOS Y CONDICIONES, sin que ello implique que le concede licencia o autorización alguna o algún tipo de derecho distinto al uso antes mencionado, respecto a la "Propiedad Intelectual" de BDP o de la que sea licenciatario. Se entiende por "Propiedad Intelectual" todas las marcas registradas y/o usadas en México o en el extranjero por BDP, así como diseños industriales, modelos de utilidad, información confidencial, nombres comerciales, avisos comerciales, reservas de derechos, nombres de dominio, así como todo tipo de derechos patrimoniales sobre obras y creaciones protegidas por derechos de autor y demás formas de propiedad industrial o intelectual reconocidas o que lleguen a reconocer las leyes correspondientes.
                <br><span class="font-bold -ml-4">12.1 NO RESPONSABILIDAD DE IDQ (DAIRY QUEEN®) Y XBOX® (MICROSOFT ®).</span><br/>
                Tanto IDQ (Dairy Queen®) como Microsoft®  y Xbox® no serán responsables por cualquier controversia que surja respecto a la dinámica de participación y en la campaña en general. Toda información recabada para fines de la dinámica es por autoría de la sociedad JUST BDP SOLUTIONS, quien será la única responsable de cualquier asunto, por lo que IDQ (Dairy Queen), Microsoft® y Xbox® se deslindan de toda responsabilidad que pudiera surgir.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">13. NEGACIÓN Y RETIRO DEL ACCESO AL PORTAL DQ + XBOX Y/O A LAS OPCIONES.</span><br/>
                BDP se reserva el derecho a negar o retirar el acceso al portal DQ + XBOX y/o al CONTENIDO en cualquier momento y sin necesidad de previo aviso, a cualquier USUARIO, a su discreción, ya sea debido al mal uso del portal DQ + XBOX o del CONTENIDO, o por incumplimiento total o parcial de los presentes TÉRMINOS Y CONDICIONES o las CONDICIONES PARTICULARES que resulten aplicables, o cualquier otra razón que BDP considere pertinente.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">14. DURACIÓN Y TERMINACIÓN.</span><br/>
                El uso del portal DQ + XBOX, tiene una duración indefinida; sin embargo BDP podrá dar por terminado, suspender o interrumpir en cualquier momento y sin necesidad de previo aviso el portal DQ + XBOX y/o el CONTENIDO u OPCIONES, incluyendo aquellas proporcionadas por cualquier PRESTADOR DE OPCIONES ADICIONALES.
                <br><span class="font-bold -ml-4">14.1 CALENDARIO DE </span><br>
                17 de agosto de 2020 al 27 de septiembre de 2020.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">15. MECANICA DE PARTICIPACIÓN</span><br/>
                1. Los Fans que realicen la compra de un Blizzard en sus presentaciones chico, mediano o grande podrán participar, (No aplica para compras por medio de Uber y Rappi).
                2.- Deberán conservar o tomar una fotografía a su ticket para validarlo en caso de resultar ganadores.
                3.- Los participantes tienen que registrarse en el sitio web http://www.datelavueltaconxbox.com  con la siguiente información:
                Facebook o llenar un formato con sus datos.
                Formato de datos: Ciudad y Nombre de sucursal
                Posteriormente todos los participantes deberán de registrar su ticket de la siguiente manera:
                Agregar número de ticket en el apartado.
                Una vez registrados comenzarán a participar por los premios semanales:
                La participación será por niveles, desde el básico para poder ganarse el premio más básico, hasta experto con la posibilidad de ganarse una consola Xbox.
                Conforme los participantes vayan respondiendo de manera correcta los retos relacionados con Xbox y DQ, decidirán si pasan al siguiente nivel para poder ganarse un mejor premio o quedarse con el premio que ya ganaron.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">16. ELEGIBILIDAD</span><br/>
                Pueden participar personas de cualquier edad, en caso de que el participante sea menor de edad, deberá de contar con autorización previa de un adulto y aceptar los presentes términos y condiciones, bajo la condición de haber cumplido con los requisitos establecidos en las redes sociales de BDP.
                No podrán participar en la dinámica toda persona empleada o trabajadora de manera directa o indirecta para BDP.
                <br><span class="font-bold -ml-4">16.1 SELECCIÓN DE GANADORES</span><br/>
                El ganador de cada premio será el primero en cumplir con todos los requisitos, contestar de manera correcta las trivias y validar su ticket de compra.
                Se elegirá un ganador con base en la participación, todas las sucursales BDP son participantes. Los premios no son intransferibles. BDP se reserva el derecho a sustituir cualquier premio por otro de valor semejante o superior. No se ofrecerá dinero ni ninguna otra alternativa.
                <br><span class="font-bold -ml-4">16.2 CONDICIÓN PARA RECLAMO DE PREMIO</span><br/>
                El ganador deberá presentar su ticket en formato original, copia o fotografía para poder reclamar su premio.
                <br><span class="font-bold -ml-4">16.3 ANUNCIAMIENTO DE LOS GANADORES</span><br/>
                Los ganadores se anunciarán los días martes de cada semana durante la duración de la dinámica.
            </div>
            <div class="mt-4 pl-4">
            <span class="font-bold -ml-4">17. OTRAS DISPOSICIONES:</span><br/>
            *Forma.
            El USUARIO acepta que tanto una versión impresa de los presentes TÉRMINOS Y CONDICIONES, y de cualquier comunicación enviada y/o recibida en forma electrónica, será admisible como medio probatorio en cualquier procedimiento judicial y/o administrativo.
            <br>*Ley aplicable y jurisdicción.
            En todo lo relacionado con la interpretación y cumplimiento de lo dispuesto en los presentes TÉRMINOS Y CONDICIONES, por el sólo hecho de acceder al portal DQ + XBOX, el USUARIO acepta someterse a las leyes aplicables y a la jurisdicción de los tribunales competentes en la Ciudad de México, México, renunciado a cualquier otra jurisdicción que por su domicilio presente o futuro, o cualquiera otra razón pudiese corresponderle.
            </div>
            <div class="mt-4 pl-4">
            <span class="font-bold -ml-4">18. DERECHOS.</span><br/>
            Cualquier derecho que no se haya conferido expresamente en este documento se entiende reservado para BDP.
            </div>
            <div class="mt-4 pl-4">
            <span class="font-bold -ml-4">19. EN CASO DE FALLECIMIENTO</span><br/>
            En caso de fallecimiento del titular de la cuenta de registro, los beneficios acumulados en su cuenta no podrán ser transferidas a ninguna otra, por lo que serán canceladas tanto la cuenta como los beneficios. Por lo tanto, los beneficios no se pueden heredar o transferir en caso de fallecimiento del usuario.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">20. MODIFICACIONES DE DATOS</span><br/>
                <span class="font-bold -ml-4">20.1 EN CUENTAS DE REGISTRO</span><br/>
                Los usuarios que cuenten con registro en el portal DQ + XBOX podrán realizar modificaciones a sus datos ingresando con su correo electrónico y contraseña. En caso de modificar el correo electrónico podrá hacerlo siempre y cuando ese correo electrónico no esté registrado. Es responsabilidad del usuario mantener actualizado sus datos y de los cambios que el mismo realice.
            </div>
            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">21. ACLARACIONES Y DUDAS</span><br/>
                Cualquier duda o aclaración deberá contactarse mediante correo electrónico a la cuenta datelavueltaconxbox@gmail.com
                Calle León Tolstoi 4944. Colonia Jardines Universidad. Zapopan. Jalisco. C.P. 45110
                Se prohíbe la reproducción total o parcial de este documento por cualquier medio sin previo y expreso consentimiento por escrito de Just BDP Solutions S.A. DE C.V. a cualquier persona y actividad que sean ajenas al mismo. CONOZCO Y ACEPTO LOS TÉRMINOS Y CONDICIONES DE USO DEL PORTAL DQ + XBOX.
            </div>

            <div class="mt-4 pl-4">
                <span class="font-bold -ml-4">Política de privacidad</span><br/>
                Esta política se basa en nuestro objetivo fundamental de ofrecer a nuestros visitantes información y recursos de calidad y al mismo tiempo mantener el anonimato individual y la privacidad personal. La siguiente declaración describe la política de privacidad del sitio web www.datelavueltaconxbox.com bajo propiedad de la sociedad JUST BDP SOLUTIONS S.A. DE C.V. en lo sucesivo ‘‘BDP’’.
                <div>
                    <span class="font-bold">Recopilación de datos de los visitantes</span><br/>
                    Cuando usted accede a la página web www.datelavueltaconxbox.com, la información sobre el usuario y la información técnica esencial y accidental es recopilada de manera automática, nos referimos a esas categorías recogidas de manera colectiva como  información de “acceso”. Ninguna otra información es recogida a través del sitio www.datelavueltaconxbox.com, excepto cuando usted deliberadamente decide revelarla (por ejemplo, pulsando una tecla para enviarnos un mensaje de correo electrónico). La información que usted puede decidir enviarnos está enumerada más adelante como “información opcional”.
                </div>
                <div>
                    <span class="font-bold">Información recopilada automáticamente</span><br/>
                    • Información del visitante: el dominio de Internet y la dirección en Internet del computador que usted está utilizando.
                    • Información técnica esencial: identificación de la página o servicio que usted está solicitando, tipo de navegador y sistema operativo que utiliza; y la fecha y hora del acceso.
                    • Información técnica no esencial: la dirección Internet del sitio web que le remitió a nosotros, y  la “información cookie” descrita más adelante.
                </div>
                <div>
                    <span class="font-bold">Información opcional</span><br/>
                    • Cuando usted nos envía un correo electrónico: su nombre, dirección de correo electrónico y el contenido del mensaje.
                    • Cuando usted llena formularios electrónicos: todos los datos que usted elige dar o confirmar.
                </div>
                <div>
                    <span class="font-bold">Ofrecer información es su elección</span><br/>
                    No existe ninguna obligación legal para que usted suministre información a nuestro sitio web. Sin embargo, nuestro sitio web no funcionará sin la información de encaminamiento y la información técnica esencial. Si su navegador no nos envía la información técnica no esencial esto no le impedirá utilizar nuestro sitio web, pero puede impedir el funcionamiento de algunas opciones. En lo que se refiere a cualquier información adicional que es solicitada al sitio web, si su búsqueda no obtiene resultados significa que esa función particular o servicio asociado con esa parte de la página web puede no está disponible para usted.
                </div>
                <div>
                    <span class="font-bold">Uso de cookies</span><br/>
                    El sitio web www.datelavueltaconxbox.com utiliza “cookies” un pequeño archivo de datos trasferido de un sitio web al disco duro de su computador, para recopilar de manera anónima información sobre el tráfico de datos. Enviamos cookies cuando usted navega en www.datelavueltaconxbox.com responde a encuestas o cuestionarios en línea, o solicita información. Aceptar cookies utilizados por nuestro sitio no nos permite acceder a su Información de Identificación Personal, pero podemos utilizar cookies para identificar su computador. La información colectiva recogida nos permite analizar modelos de tráfico en nuestro sitio. Esto nos ayuda a ofrecerle un mejor servicio al mejorar el contenido y hacer más fácil el uso de nuestro sitio.
                </div>
                <div>
                    <span class="font-bold">Cómo utilizamos la información</span><br/>
                    La información del visitador es utilizada para enviar la página web a su computador para su visualización. En teoría la página web solicitada y la información del encaminamiento podrían ser descifradas por otras entidades involucradas en la transmisión de la página. No controlamos las prácticas de privacidad de esas entidades. La información técnica esencial y no esencial nos ayuda a responder a su solicitud en un formato apropiado (o de manera personalizada) y nos ayuda a planificar mejoras a nuestro sitio web. La información adicional nos permite ofrecer servicios o información más ajustada a sus necesidades o para responder a su mensaje o consultar a otra organización más competente, y también para planificar mejoras de nuestro sitio web.
                    Es posible que conservemos la información de acceso en nuestros sistemas por un tiempo indefinido después que la página es transmitida, pero no intentaremos obtener ninguna información para vincularla a las personas que navegan en nuestro sitio. Sin embargo, en las pocas ocasiones en que un “hacker” intenta violar la seguridad, los registros de información de acceso se mantienen para permitir una investigación de seguridad, y en estos casos puede ser enviada junto a cualquier otra información pertinente en nuestro poder a las autoridades judiciales.
                    Utilizamos la información que nos suministra sólo para satisfacer su específica solicitud. No compartimos esta información con terceros, salvo en la medida en que sea necesario para completar tal solicitud. De igual manera, utilizamos la información que nos suministra sobre otra persona al hacer una solicitud sólo para completar esa solicitud. De nuevo, no compartimos esta información con terceros excepto en la medida en que sea necesario para completar tal solicitud.
                    Por lo general utilizamos las direcciones de correo electrónico sólo para responder a los mensajes que recibimos. Estas direcciones en general no son utilizadas para ningún otro propósito.
                    En fin, no utilizamos o compartimos nunca la información de identificación personal que nos es suministrada en línea en otras formas que las descritas antes sin una clara notificación sobre el sitio en particular y sin ofrecerle la oportunidad de excluir o prohibir este tipo de uso.
                </div>
                <div>
                    <span class="font-bold">Nuestro compromiso para la protección de los datos</span><br/>
                    BDP no vende ninguna información de identificación personal ofrecida voluntariamente en el sitio www.datelavueltaconxbox.com a terceras partes. Para prevenir el acceso no autorizado, mantener la exactitud de los datos, y garantizar el uso correcto de a información, hemos instaurado procedimientos físicos, electrónicos y administrativos para salvaguardar y proteger la información que recogemos en línea, de acuerdo con las políticas de www.datelavueltaconxbox.com.
                    Todos los empleados de BDP que tienen acceso al tratamiento o intervienen en el procesamiento de datos personales, están obligados a respetar la confidencialidad de los asuntos oficiales, incluyendo los datos personales.
                </div>
                <div>
                    <span class="font-bold">Cómo contactarnos</span><br/>
                    Si tiene otras preguntas o preocupaciones sobre nuestras políticas y prácticas de privacidad, por favor envíe un correo electrónico a datelavueltaconxbox@gmail.com. Calle León Tolstoi 4944. Col Jardines Universidad 45110. Zapopan, Jalisco. México.
                </div>
            </div>


            <div class="mt-10">
                Última actualización 1p de Agosto de 2020.
            </div>
    	</div>
    </div>
</div>
