@section('title', 'Date la vuelta con xbox')

<div>
    <x-titulo_principal fish=true/>

    <div class="mt-16 mx-auto w-full max-w-sm md:max-w-lg lg:max-w-3xl border-b-2 border-dashed border-x_green_lightest relative">
        <div class="text-center mt-16 py-10">
            <x-input.link_button class="relative z-10 inline-block text-white mx-auto font-amsi bg-x_green_lightest py-4 px-12 md:px-16 hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" href="/registro" texto="Regístrate aquí"/>
            <a class="mx-auto w-64 py-4 text-white uppercase font-amsi mt-6 text-center text-tiny bg-x_blue hover:bg-blue-700 transition-all duration-200 ease-in flex items-center justify-center" href="/login/facebook">
                <img class="mr-2 w-5 sm:w-6" src="{{ asset('images/facebook.png') }}" alt=""> Conectar con Facebook
            </a>
            <p class="font-amsi md:text-base mt-10 normal-case text-white">¿Ya eres usuario?, <a class="underline" href="{{ route('login') }}">Inicia sesión</a></p>
        </div><!-- Links -->

        <div class="flex flex-col md:flex-row items-center justify-center w-full sm:w-11/12 lg:w-9/12 py-16 mx-auto relative" style="background-image: radial-gradient(ellipse at 50% 50%, #186608, #000000 75%);">
            <img class="absolute w-6 right-0 top-0 mt-20 mr-20" src="{{ asset('images/x_titulo.svg') }}" alt="">
            <div class="xbox_reward">
                <img class="w-5/12 mx-auto" src="{{ asset('images/logo_xbox_alt.svg') }}" alt="">
                <img class="mt-4" src="{{ asset('images/premio_xbox.png') }}" alt="" srcset="">
            </div>
        </div><!-- Premios -->
    </div>


    <div class="w-10/12 md:w-11/12 mx-auto text-center text-lg md:text-xl lg:text-2xl leading-tight font-industry text-white mt-16">
        <p>Entre más tickets registres más oportunidades tienes de <br> ganar uno de nuestros premios semanales.</p>
    </div><!-- Dinamica -->

    @livewire('contador-premios')

</div>
