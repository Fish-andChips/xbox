<div>
    <x-titulo_perfil/>

    <div class="mt-16 text-white">
        <div class="w-16 h-16 md:w-24 md:h-24 rounded-full bg-blue-600 mx-auto flex flex-col items-center justify-center">
            <h2 class="font-arial font-bold text-3xl md:text-5xl leading-none">{{substr(Auth::user()->name, 0,1)}}</h2>
        </div>
    </div>
    <div class="mx-auto w-full max-w-xs sm:max-w-sm md:max-w-xl lg:max-w-2xl mt-16">
        <form wire:submit.prevent="updateProfile">
            <div class="flex flex-col md:flex-row items-star justify-between">
                <div class="px-1 md:px-4 w-full md:w-1/2">
                    <x-input.text wire:model.lazy="nombre" oninput="this.value=this.value.replace(/[0-9]/g,'');" id="nombre" name="nombre" placeholder="Nombre" :error="$errors->first('nombre')" autofocus/>
                </div>
                <div class="px-1 md:px-4 w-full md:w-1/2">
                    <x-input.text wire:model.lazy="apellido" oninput="this.value=this.value.replace(/[0-9]/g,'');" id="apellido" name="apellido" placeholder="Apellido" :error="$errors->first('apellido')"/>
                </div>
            </div>
            <div class="flex flex-col md:flex-row items-star justify-between">
                <div class="px-1 md:px-4 w-full md:w-1/2">
                    <x-input.text wire:model.lazy="telefono" maxlength="10" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="telefono" name="telefono" placeholder="Teléfono (opcional)" :error="$errors->first('telefono')"/>
                </div>
                <div class="px-1 md:px-4 w-full md:w-1/2">
                    <x-input.text wire:model.lazy="email" id="email" name="email" placeholder="Correo electrónico" :error="$errors->first('email')"/>
                </div>
            </div>
            <div class="flex flex-col md:flex-row items-star justify-between">
                <div class="px-1 md:px-4 w-full md:w-1/2">
                    <x-input.date wire:model.lazy="fecha_nacimiento" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Fecha de nacimiento" label="Fecha de nacimiento DD/MM/YYYY" :error="$errors->first('fecha_nacimiento')"/>
                </div>
                <div class="px-1 md:px-4 w-full md:w-1/2">
                    <x-input.text wire:model.lazy="ciudad" id="ciudad" name="ciudad" placeholder="Ciudad" :error="$errors->first('ciudad')"/>
                </div>
            </div>
            <div class="flex flex-col md:flex-row items-star justify-between">
                <div class="px-1 md:px-4 w-full md:w-1/2">
                    <x-input.text wire:model.lazy="codigo_postal" maxlength="5" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="codigo_postal" name="codigo_postal" placeholder="Código postal" :error="$errors->first('codigo_postal')"/>
                </div>
                <div class="px-1 md:px-4 w-full md:w-1/2">
                    <x-input.password wire:model.lazy="password" id="password" name="password" placeholder="Contraseña" :error="$errors->first('password')"/>
                </div>
            </div>

            <div class="mt-16 mx-auto max-w-sm">
                <x-input.button type="submit" class="bg-x_green_lightest w-full py-6 text-x_green uppercase font-amsi md:text-2xl hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" texto="Guardar"/>
                <x-input.link_button class="w-full block py-6 border-white border-2 text-white uppercase font-amsi mt-6 text-center md:text-2xl hover:bg-white hover:text-x_green transition-all duration-200 ease-in" href="{{ route('perfil')}}" texto="Cancelar"/>
            </div>

        </form>
    </div>
</div>

