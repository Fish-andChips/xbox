<div x-data="{open:false}" @click.away="open = false" class="absolute right-0 top-0 mr-4 -mt-12 sm:text-white z-50">
    @if($avatar)
	<div x-on:click="open = !open" class="w-10 h-10 md:w-12 md:h-12 rounded-full shadow-xl border-red-900 border-2 mx-auto cursor-pointer overflow-hidden">
    	<img src="{{ $avatar }}" alt="">
    </div>
    @else
    <div x-on:click="open = !open" class="w-10 h-10 md:w-12 md:h-12 rounded-full shadow-xl border-red-900 bg-x_red border-2 mx-auto flex items-center justify-center cursor-pointer">
    	<p class="font-arial font-bold text-xl md:text-2xl">{{ substr(auth()->user()->name, 0, 1) }}</p>
    </div>
    @endif
    <div x-show="open"
        x-transition:enter="transition ease-out duration-100"
        x-transition:enter-start="opacity-0 transform scale-90"
        x-transition:enter-end="opacity-100 transform scale-100"
        x-transition:leave="transition ease-in duration-100"
        x-transition:leave-start="opacity-100 transform scale-100"
        x-transition:leave-end="opacity-0 transform scale-90"
        class="-ml-24 mt-3 flex flex-col items-start bg-white shadow-md text-green-700 py-2 px-4 rounded-lg normal-case">
        <a class="text-sm md:text-base py-3 font-bold hover:text-gray-800" href="{{ route('perfil') }}">Mi Perfil</a>
        <a class="text-sm md:text-base py-3 font-bold hover:text-gray-800 {{ (session('ticket')? 'text-gray-400 pointer-events-none': '')}}" href="{{ route('tickets') }}">Tickets</a>
        <a class="text-sm md:text-base py-3 font-bold hover:text-gray-800 {{ (!session('ticket')? 'text-gray-400 pointer-events-none': '')}}" href="{{ route('trivias') }}">Trivias</a>
    	<a class="text-sm md:text-base py-3 font-bold text-x_red hover:text-red-800"
    	   href="{{ route('logout') }}"
    	   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar sesión</a>
    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>
