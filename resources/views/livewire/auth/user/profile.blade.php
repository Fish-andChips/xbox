<div>
    <x-titulo_perfil/>

    <div class="mx-auto w-11/12 sm:max-w-lg md:max-w-xl text-white">
        <div class="mt-16">
            @if($avatar)
            <div class="w-16 h-16 md:w-24 md:h-24 rounded-full mx-auto flex items-center justify-center overflow-hidden">
                <img src="{{ $avatar }}" alt="">
            </div>
            @else
            <div class="w-16 h-16 md:w-24 md:h-24 rounded-full bg-blue-600 mx-auto flex items-center justify-center">
                <p class="font-arial font-bold text-3xl md:text-5xl leading-none">{{substr($usuario['nombre'], 0,1)}}</p>
            </div>
            @endif
            <p class="text-center font-arial text-lg md:text-2xl mt-8 normal-case">
                <a href="{{ route('editar_perfil') }}" class="inline cursor-pointer">Editar <img class="w-4 md:w-6 inline-block ml-4 align-baseline" src="{{ asset('images/editar.png') }}" alt=""></a>
            </p>
        </div>

        <div class="flex flex-col mt-16 normal-case">
            <div class="flex flex-row items-end justify-start lg:justify-center mt-8">
                <div class="w-1/2 px-2">
                    <span class="font-arial text-md sm:text-lg md:text-2xl">{{ $usuario['nombre'] }}</span>
                    <span class="block mt-1 text-xs text-green-200 font-bold">Nombre</span>
                </div>
                <div class="w-1/2 px-2">
                    <span class="font-arial text-md sm:text-lg md:text-2xl">{{ $usuario['apellido'] }}</span>
                    <span class="block mt-1 text-xs text-green-200 font-bold">Apellido</span>
                </div>
            </div>
            <div class="flex flex-row items-end justify-start mt-8">
                <div class="w-1/2 px-2">
                    <span class="font-arial text-md sm:text-lg md:text-2xl">{{ $usuario['telefono'] }}</span>
                    <span class="block mt-1 text-xs text-green-200 font-bold">Teléfono</span>
                </div>
                <div class="w-1/2 px-2">
                    <span class="font-arial text-md sm:text-lg md:text-2xl">{{ $usuario['email'] }}</span>
                    <span class="block mt-1 text-xs text-green-200 font-bold">Correo electrónico</span>
                </div>
            </div>
            <div class="flex flex-row items-end justify-start mt-8">
                <div class="w-1/2 px-2">
                    <span class="font-arial text-md sm:text-lg md:text-2xl">{{ $usuario['fecha_nacimiento'] }}</span>
                    <span class="block mt-1 text-xs text-green-200 font-bold">Fecha de nacimiento DD/MM/YYYY</span>
                </div>
                <div class="w-1/2 px-2">
                    <span class="font-arial text-md sm:text-lg md:text-2xl">{{ $usuario['ciudad'] }}</span>
                    <span class="block mt-1 text-xs text-green-200 font-bold">ciudad</span>
                </div>
            </div>
            <div class="flex flex-row items-end justify-start mt-8">
                <div class="w-1/2 px-2">
                    <span class="font-arial text-md sm:text-lg md:text-2xl">{{ $usuario['codigo_postal'] }}</span>
                    <span class="block mt-1 text-xs text-green-200 font-bold">Codigo postal</span>
                </div>
                <div class="w-1/2 px-2">
                    <span class="font-arial text-md sm:text-lg md:text-2xl">**********</span>
                    <span class="block mt-1 text-xs text-green-200 font-bold">Contraseña</span>
                </div>
            </div>
        </div><!-- datos del usuario -->
    </div>
</div>
