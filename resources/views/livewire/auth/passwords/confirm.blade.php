@section('title', 'Confirma tu contraseña')


<div>
    <div class="mt-12 mx-auto w-11/12 sm:max-w-lg md:max-w-xl text-center font-industry">
        <div class="py-3 bg-x_green_lightest">
            <h1 class="text-2xl sm:text-4xl md:text-5xl leading-none text-x_green">Confirma tu contraseña</h1>
        </div>
    </div>

    <div class="mx-auto w-full max-w-sm mt-16">
        <form wire:submit.prevent="confirm">
            <x-input.password wire:model.lazy="password" id="password" name="password" placeholder="Contraseña" :error="$errors->first('password')"/>
            <div class="text-lg md:text-2xl normal-case leading-5 text-center mt-6 text-white">
                    <p class="py-2">¿Olvidaste tu contraseña?</p>
                    <a href="{{ route('password.request') }}" class="underline">
                        Restablecer contraseña
                    </a>
                </div>
            <div class="mt-16">
                <x-input.button type="submit" class="bg-x_green_lightest w-full py-6 text-x_green uppercase font-amsi md:text-2xl hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" texto="confirmar contraseña"/>
            </div>
        </form>
    </div>
</div>
