@section('title', 'Reestablecer contraseña')

<div>
    <x-titulo_restablecer hide="false" />

    <div class="mt-16 mx-auto max-w-sm sm:max-w-lg lg:max-w-3xl">
        <p class="font-industry text-xl sm:text-2xl md:text-4xl text-center text-white">Ingresa tu correo electrónico y la nueva contraseña.</p>
    </div>

    <div class="mx-auto w-full max-w-xs md:max-w-md mt-16">
        <form wire:submit.prevent="resetPassword">
            <input wire:model="token" type="hidden">
            <x-input.text wire:model.lazy="email" id="email" name="email" placeholder="Correo electrónico" :error="$errors->first('email')"/>
            <x-input.password wire:model.lazy="password" id="password" name="password" placeholder="Contraseña" :error="$errors->first('password')"/>
            <x-input.password wire:model.lazy="passwordConfirmation" id="passwordConfirmation" name="passwordConfirmation" placeholder="Confirmar contraseña" :error="$errors->first('confirm_password')"/>

            <div class="mt-12">
                <x-input.button type="submit" class="bg-x_green_lightest w-full py-6 text-x_green uppercase font-amsi md:text-2xl hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" texto="restablecer contraseña"/>
            </div>
        </form>
    </div>
</div>

