@section('title', 'Recuperar contraseña')

<div>

    <x-titulo_restablecer hide="{{$emailSentMessage ? '1' : '0' }}" />

    @if ($emailSentMessage)
        <div class="mt-16 mx-auto max-w-sm sm:max-w-xl">
            <p class="font-industry text-lg sm:text-xl md:text-3xl text-center text-white">Revisa tu correo, te hemos mandado los datos necesarios para seguir en el juego.</p>
            <div class="max-w-sm mx-auto mt-24">
                <x-input.link_button class="block text-center mx-auto bg-x_green_lightest w-full py-6 text-x_green uppercase font-amsi md:text-2xl hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" href="{{ route('home')}}" texto="Regresar al home"/>
            </div>

        </div>
    @else
        <div class="mx-auto w-full max-w-xs md:max-w-sm mt-16">
            <form wire:submit.prevent="sendResetPasswordLink">
                <x-input.text wire:model.lazy="email" id="email" name="email" placeholder="Correo electrónico" :error="$errors->first('email')"/>
                <x-input.text wire:model.lazy="email_confirmation" id="email_confirmation" name="email_confirmation" placeholder="Confirma tu correo electrónico"/>

                <div class="mt-12">
                    <x-input.button type="submit" class="bg-x_green_lightest w-full py-6 text-x_green uppercase font-amsi md:text-2xl hover:bg-x_green_light hover:text-white transition-all duration-200 ease-in" texto="restablecer"/>
                    <x-input.link_button class="w-full block py-6 border-white border-2 text-white uppercase font-amsi mt-6 text-center md:text-2xl hover:bg-white hover:text-x_green transition-all duration-200 ease-in" href="{{ route('home')}}" texto="Cancelar"/>
                </div>

            </form>
        </div>
    @endif

</div>
