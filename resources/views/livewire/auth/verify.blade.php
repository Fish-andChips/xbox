@section('title', 'Verifica tu correo electrónico')

<div>
    <div class="mx-auto w-full max-w-xs sm:max-w-md md:max-w-xl">

        <div class="mt-12 mx-auto w-11/12 max-w-xs sm:max-w-lg md:max-w-xl text-center font-industry">
            <div class="py-3 bg-x_green_lightest">
                <h2 class="text-2xl sm:text-4xl md:text-5xl leading-none text-x_green">Verifica tu correo electrónico</h2>
            </div>
        </div>

    </div>

    <div class="mt-8 mx-auto w-full max-w-xs sm:max-w-md md:max-w-lg">
        <div class="px-4 py-8 sm:px-10">
            @if (session('resent'))
                <div class="flex items-center px-4 py-3 mb-6 text-sm text-white bg-x_green_light rounded shadow normal-case" role="alert">
                    <svg class="w-4 h-4 mr-3 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path>
                    </svg>
                    <p>Te hemos enviado un nuevo link de verificación a tu correo</p>
                </div>
            @endif

            @if (session('error_send'))
                <div class="flex items-center px-4 py-3 mb-6 text-sm text-white bg-x_red rounded shadow normal-case" role="alert">
                    <p>Ha habido un error al realizar el envío del correo, inténtalo mas tarde</p>
                </div>
            @endif

            <div class="text-sm md:text-lg text-white font-amsi normal-case">

                <p>Antes de continuar, por favor revisa tu correo, te hemos enviado un link para verificar tu cuenta</p>

                <p class="mt-3">
                    Si no recibiste el correo, <a wire:click="resend" class="text-x_green_lightest cursor-pointer hover:text-x_green_light focus:outline-none focus:underline transition ease-in-out duration-150">haz click aquí para enviarte otro</a>.
                </p>
            </div>
        </div>
    </div>
</div>
