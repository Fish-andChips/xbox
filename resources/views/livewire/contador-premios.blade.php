<div @if(Request::is('/')) style="background: linear-gradient(#0000 50%, #186608 50%)" @endif>
    <div class="mx-auto w-full sm:max-w-lg md:max-w-2xl lg:max-w-screen-lg mt-16">
        <div class="flex flex-row flex-wrap items-start font-industry">
            @if(!$premios)
                <div class="w-1/2 md:w-1/4 flex flex-col items-center text-xl md:text-2xl lg:text-3xl text-x_green_lightest p-8">
                    <p class="leading-none">0</p>
                    <p>Códigos</p>
                    <img class="mt-8 w-40" src="{{ asset('images/logo_xbox_alt.svg')}}" alt="">
                    <p class="text-lg lg:text-xl text-white mt-2">14 días</p>
                </div>
                <div class="w-1/2 md:w-1/4 flex flex-col items-center text-xl md:text-2xl lg:text-3xl text-x_green_lightest p-8">
                    <p class="leading-none">0</p>
                    <p>Códigos</p>
                    <img class="mt-8 w-40" src="{{ asset('images/logo_xbox_alt.svg')}}" alt="">
                    <p class="text-lg lg:text-xl text-white mt-2">1 mes</p>
                </div>
                <div class="w-1/2 md:w-1/4 flex flex-col items-center text-xl md:text-2xl lg:text-3xl text-x_green_lightest p-8">
                    <p class="leading-none">0</p>
                    <p>Códigos</p>
                    <img class="mt-8 w-40" src="{{ asset('images/logo_xbox_alt.svg')}}" alt="">
                    <p class="text-lg lg:text-xl text-white mt-2">3 meses</p>
                </div>
                <div class="w-1/2 md:w-1/4 flex flex-col items-center text-xl md:text-2xl lg:text-3xl text-x_green_lightest p-8">
                    <p class="leading-none">0</p>
                    <p>Consolas</p>
                    <img class="mt-8 w-40" src="{{ asset('images/premio_xbox.png')}}" alt="">
                </div>
            @else
                <div class="w-1/2 md:w-1/4 flex flex-col items-center text-xl md:text-2xl lg:text-3xl text-x_green_lightest p-8">
                    <p class="leading-none">{{ $premios[0]->quantity }}</p>
                    <p>Códigos</p>
                    <img class="mt-8 w-40" src="{{ asset('images/logo_xbox_alt.svg')}}" alt="">
                    <p class="text-lg lg:text-xl text-white mt-2">14 días</p>
                </div>
                <div class="w-1/2 md:w-1/4 flex flex-col items-center text-xl md:text-2xl lg:text-3xl text-x_green_lightest p-8">
                    <p class="leading-none">{{ $premios[1]->quantity }}</p>
                    <p>Códigos</p>
                    <img class="mt-8 w-40" src="{{ asset('images/logo_xbox_alt.svg')}}" alt="">
                    <p class="text-lg lg:text-xl text-white mt-2">1 mes</p>
                </div>
                <div class="w-1/2 md:w-1/4 flex flex-col items-center text-xl md:text-2xl lg:text-3xl text-x_green_lightest p-8">
                    <p class="leading-none">{{ $premios[2]->quantity }}</p>
                    <p>Códigos</p>
                    <img class="mt-8 w-40" src="{{ asset('images/logo_xbox_alt.svg')}}" alt="">
                    <p class="text-lg lg:text-xl text-white mt-2">3 meses</p>
                </div>
                <div class="w-1/2 md:w-1/4 flex flex-col items-center text-xl md:text-2xl lg:text-3xl text-x_green_lightest p-8">
                    <p class="leading-none">{{ $premios[3]->quantity }}</p>
                    <p>{{($premios[3]->quantity != 1) ? 'consolas' : 'consola' }}</p>
                    <img class="mt-8 w-40" src="{{ asset('images/premio_xbox.png')}}" alt="">
                </div>
            @endif
        </div><!-- Premios a repartir -->
    </div>
</div>
