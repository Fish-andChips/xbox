<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Email</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<style type="text/css">
		* {
			box-sizing: border-box;
		}
		.link {
			color: #FFFFFF;
			text-decoration: none;
			font-weight: bold;
			padding: 0 50px;
		}
	</style>
</head>
<body width="100%"
style="margin: 0; padding: 0 !important;
mso-line-height-rule: exactly;
background-color: #186608;
background-image: radial-gradient(circle at 48% 50%, #186608, #000000 95%);
background-size: cover;
background-attachment: fixed;
color: #ffff;
font-family: sans-serif;
">
<table cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td>
			<table align="center" cellpadding="100" cellspacing="0" width="600"  style="border-collapse: collapse;">
				<tr>
					<td style="padding: 100px 50px">
						<table align="center" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center">
									<img style="display: block" src="https://datelavueltaconxbox.com/images/header_email.png" alt="" />
								</td>
                            </tr>
                            <tr>
								<td style="padding:80px 0;" align="center">
									<img style="display: block" src="https://datelavueltaconxbox.com/images/premios_email.png" alt="" />
								</td>
							</tr>
							<tr>
								<td style="padding:40px 0 50px 0;" align="center">
									<img style="display: block" src="https://datelavueltaconxbox.com/images/restablecel_password.png" alt="" />
									<img style="display: block; margin-top: 60px;" src="https://datelavueltaconxbox.com/images/icon_restableceremail.png" alt="" />
								</td>
							</tr>
							<tr>
								<td>
									<p style="font-size: 24px">Si deseas recuperar tu contraseña, por favor haz click  en el siguiente enlace</p>
									<a style="font-weight: normal; color: #ffffff; display: block; width: 600px; word-wrap: break-word;" href="{{$url}}">
										{{$url}}
									</a>
								</td>
							</tr>
							<tr>
								<td style="padding: 100px 0 50px 0;">
									<p style="font-size: 24px">
										Si no has solicitado un cambio de contraseña, puedes ignorar o eliminar este e-mail.
									</p>
									<p style="font-size: 24px">Saludos,</p>
									<img src="https://datelavueltaconxbox.com/images/logo_dq.png" alt="" />
								</td>
							</tr>
							<tr>
								<td style="padding-top: 80px;" align="center">
									<a class="link" target="_blank" href="https://datelavueltaconxbox.com/terminos">Términos y condiciones</a>
									<a class="link" target="_blank" href="https://datelavueltaconxbox.com/faqs">FAQ'S</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
