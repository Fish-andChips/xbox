<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Email</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<style type="text/css">
		* {
			box-sizing: border-box;
		}
		.link {
			color: #FFFFFF;
			text-decoration: none;
			font-weight: bold;
			padding: 0 50px;
		}
	</style>
</head>
<body width="100%"
style="margin: 0; padding: 0 !important;
mso-line-height-rule: exactly;
background-color: #186608;
background-image: radial-gradient(circle at 48% 50%, #186608, #000000 95%);
background-size: cover;
background-attachment: fixed;
color: #ffff;
font-family: sans-serif;
">
<table cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td>
			<table align="center" cellpadding="0" cellspacing="0" width="600"  style="border-collapse: collapse;">
				<tr>
					<td style="padding: 100px 50px;">
						<table align="center" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center">
									<img style="display: block" src="https://datelavueltaconxbox.com/images/header_email.png" alt="" />
								</td>
							</tr>
							<tr>
								<td style="padding:140px 0 50px 0;" align="center">
									<img style="display: block" src="https://datelavueltaconxbox.com/images/email_gracias.png" alt="" />
									<img style="display: block; height: 32px; vertical-align: middle; margin-top: 40px;" src="https://datelavueltaconxbox.com/images/logo_xbox.png" alt="" />
									<img style="display: block; height: 20x; vertical-align: baseline; margin-top: 15px;" src="https://datelavueltaconxbox.com/images/email_3_meses.png" alt="" />
									<img style="display: block; margin-top: 20px" src="https://datelavueltaconxbox.com/images/gamepass-white.png" alt="" />
								</td>
							</tr>
							<tr>
								<td>
									<p style="font-size: 24px; letter-spacing: 0.01rem; word-spacing: 5px;">
										<b style="margin-bottom: 20px;">Cumpliste la misión</b><br /><br />
										Por favor responde este correo con la foto del ticket con el que participaste para verificar tus datos. <br /><br />
										¡Muy pronto podrás saber si eres el ganador de uno de nuestros <img style="display: inline-block; vertical-align: baseline;" src="https://datelavueltaconxbox.com/images/18mil.png" alt="" />!
									</p>
								</td>
                            </tr>
                            <tr>
								<td>
									<p style="font-size: 24px; letter-spacing: 0.01rem; word-spacing: 5px;">Recuerda que tienes 24 horas a partir de terminaste las trivias para enviarnos la fotografía de tu ticket a <span style="color: #24A112;">contacto@datelavueltaconxbox.com</span> para verificar tus datos y así puedas reclamar tu premio.</p>
									<p style="font-size: 24px; letter-spacing: 0.01rem; word-spacing: 5px;">No olvides que en caso de no cumplir con este paso, tu participación quedará eliminada.</p>
								</td>
							</tr>
							<tr>
								<td style="padding: 50px 0 50px 0;">
									<p style="font-size: 24px">Saludos,</p>
									<img style="display: block" src="https://datelavueltaconxbox.com/images/logo_dq.png" alt="" />
								</td>
							</tr>
							<tr>
								<td style="padding-top: 80px;" align="center">
									<a class="link" target="_blank" href="https://datelavueltaconxbox.com/terminos">Términos y condiciones</a>
									<a class="link" target="_blank" href="https://datelavueltaconxbox.com/faqs">FAQ'S</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
