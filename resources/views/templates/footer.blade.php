<footer class="p-2 mt-16 main-footer max-w-screen-sm md:max-w-2xl lg:max-w-screen-md xl:max-w-screen-lg mx-auto">

	<div class="flex items-center justify-center font-arial text-tiny font-bold normal-case text-white">
        <a class="px-8" href="/terminos">Términos y condiciones</a>
        <a class="px-8" href="/faqs">FAQ's</a>
    </div><!-- Terminos/FAQS -->

    <p class="text-white text-xs sm:text-md md:text-tiny text-center normal-case font-narrow mt-3">
        Los ganadores serán notificados por medio de correo electrónico. Promoción válida del 17 de Agosto al 27 de Septiembre del 2020 en todas las sucursales de DQ® de la República Mexicana. Válido solo en la compra de un Blizzard® (No aplica tamaño Mini y Familiar). <a href="/terminos">Consulta los términos y condiciones.</a>
    </p>
</footer>
