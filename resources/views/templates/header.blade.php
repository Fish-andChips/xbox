<header class="relative">
    @if(Request::is('/'))
        <div class="bg-x_red h-2"></div>
    @endif
    <div class="w-full mx-auto max-w-5xl mt-16 main-header relative">
    	<a class="inline-block mx-auto max-w-md flex flex-row items-center justify-center" href="{{ route('home') }}">
        	<img class="w-20 sm:w-24 lg:w-auto" src="{{ asset('images/logo_dq.png') }}" alt="">
        </a>
        <div>
            @auth
                @livewire('auth.user.profile-menu')
            @endauth
        </div>

        <!-- Notificacion -->
        @if(Session::has('message'))
            <div x-data="{open:true}" x-init="setTimeout(function(){ open = false},2000)" >
                <div
                x-show="open"
                x-transition:enter="transition ease-out duration-300"
                x-transition:enter-start="opacity-0 transform scale-90"
                x-transition:enter-end="opacity-100 transform scale-100"
                x-transition:leave="transition ease-in duration-300"
                x-transition:leave-start="opacity-100 transform scale-100"
                x-transition:leave-end="opacity-0 transform scale-90"
                class="absolute top-0 right-0 z-10 mr-4 max-w-xs">
                    <div class="m-auto">
                        <div class="bg-white rounded-lg border-gray-300 border p-3 shadow-lg">
                            <div class="flex flex-row">
                                <div class="ml-2 mr-6 normal-case text-xs font-arial">
                                    <span class="font-semibold">{{ Session::get('titulo') }}</span>
                                    <span class="block text-gray-500">{{ Session::get('message') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</header>
