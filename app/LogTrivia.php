<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogTrivia extends Model
{
    protected $table = 'log_trivia';

    protected $fillable = [
        'user_id', 'ticket_id', 'trivia_id', 'question', 'answer', 'answer_id', 'is_correct', 'start_trivia', 'end_trivia'
    ];
}
