<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'ticket', 'user_id', 'store', 'week_id', 'is_valid'
    ];


    /**
     * Relacion con tabla User
     */
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function week()
    {
    	return $this->belongsTo('App\Week');
    }

    public function winner()
    {
        return $this->hasOne('App\Winner');
    }
}
