<?php

//Obtener el número de semana
if(!function_exists('current_week'))
{
	function current_week()
	{
		$today = date('Y-m-d');
		$week = DB::table('weeks')
				->select('id')
				->where('start_date', '<=', $today)
				->where('end_date', '>=', $today)
                ->first();
        if($week)
            return $week->id;

        return false;
	}
}

if(!function_exists('contador_premios'))
{
	function contador_premios()
	{
        $week = current_week();
        if(!$week)
            return false;

		$premios_db = DB::table('weekly_rewards')
                   ->select('reward_id', 'quantity', 'reward', 'weekly_rewards.level')
                   ->join('rewards', 'rewards.id', '=', 'weekly_rewards.reward_id')
				   ->where('week_id', $week)
				   ->get();

		return $premios_db->toArray();
	}
}

if(!function_exists('obtener_ruta_login'))
{
    function obtener_ruta_login()
    {
        //Obtener el ticket
        $ticket = existe_ticket();
        if($ticket)
            return 'trivias';
        return 'tickets';
    }
}

if(!function_exists('existe_ticket'))
{
    function existe_ticket()
    {
        //Obtener semana
        $week = current_week();
        //Obtener el ticket
        $ticket = DB::table('tickets')
                    ->select('id', 'week_id', 'is_valid', 'level')
                    ->where('user_id', auth()->user()->id)
                    ->get()->last();

        if(!$ticket)
        {
            return false;
        }

        // Validación del ticket
        if($ticket->is_valid && $ticket->week_id == $week)
        {
            session(['ticket' => $ticket->id, 'nivel' => $ticket->level]);
            return true;
        }

        if($ticket->is_valid && $ticket->week_id != $week)
        {
            invalidar_ticket($ticket->id);
        }

        return false;
    }
}

if(!function_exists('invalidar_ticket'))
{
    function invalidar_ticket($ticket_id)
    {
        //Obtener el ticket
        $ticket = App\Ticket::find($ticket_id);
        $ticket->is_valid = 0;
        $ticket->save();

        session()->forget('ticket');
        session()->forget('nivel');
        //$ticket = DB::table('tickets')->where('id', $ticket_id)->update(['is_valid' => 0]);
    }
}

if(!function_exists('obtener_trivias'))
{
    function obtener_trivias($nivel, $numero_preguntas)
    {
        //Obtener semana
        $week = current_week();
        $trivias = App\Trivia::select('id','question', 'time_limit', 'correct', 'answers')
            ->where('level', $nivel)
            ->where('week', $week)
            ->inRandomOrder()
            ->limit($numero_preguntas)
            ->get()->toArray();

            return $trivias;
    }
}

if(!function_exists('subir_nivel'))
{
    function subir_nivel($ticket_id)
    {
        //Obtener el ticket
        $ticket = App\Ticket::find($ticket_id);

        //Obtener premios de siguiente nivel
        $siguiente_nivel = obtener_siguiente_nivel($ticket->level);

        if(!$siguiente_nivel)
        {
            return false;
        }

        $ticket->level = $siguiente_nivel;
        $ticket->save();

        session(['nivel' => $ticket->level]);
        return true;
        //$ticket = DB::table('tickets')->where('id', $ticket_id)->update(['is_valid' => 0]);
    }
}

if(!function_exists('obtener_siguiente_nivel'))
{
    function obtener_siguiente_nivel($nivel)
    {
        $premios = contador_premios();
        //dd($premios);
        $siguiente = $nivel + 1;
        //Ya es el último nivel
        if($siguiente > 4)
        {
            return false;
        }

        if($premios[$siguiente-1]->quantity >= 1)
        {
            return $premios[$siguiente-1]->level;
        }

        return obtener_siguiente_nivel($siguiente);
    }
}

if(!function_exists('iniciar_log_trivia'))
{
    function iniciar_log_trivia($trivia, $pregunta)
    {
        //Obtener el ticket
        $trivia = App\LogTrivia::create([
            'user_id' => auth()->user()->id,
            'ticket_id' => session('ticket'),
            'trivia_id' => $trivia,
            'question' => $pregunta,
            'start_trivia' => date('Y-m-d H:i:s'),
            'end_trivia' => date('Y-m-d H:i:s')

        ]);

        return $trivia->id;
    }
}

if(!function_exists('terminar_log_trivia'))
{
    function terminar_log_trivia($trivia, $respuesta, $texto, $correcta)
    {
        //Obtener log
        $log = App\LogTrivia::find($trivia);
        $log->answer = $texto;
        $log->answer_id = $respuesta;
        $log->is_correct = $correcta;
        $log->end_trivia = date('Y-m-d H:i:s');
        $log->save();
    }
}

if(!function_exists('registrar_ganador'))
{
    function registrar_ganador()
    {
        $week = current_week();
        if(!$week)
            return false;

        $premio_nombre = App\Reward::select('reward')->where('level', session('nivel'))->first();
        //Bloquear la DB para guardar el premio correctamente
        DB::beginTransaction();
        try {
            App\Winner::create([
                'user_id' => auth()->user()->id,
                'ticket_id' => session('ticket'),
                'level' => session('nivel'),
                'reward_id' => session('nivel') ,
                'week_id' => $week,
                'reward_name' => $premio_nombre->reward
            ]);

            //Restar premio
            App\WeeklyReward::where('week_id', $week)->where('reward_id', session('nivel'))->decrement('quantity');
        }
        catch(Exception $e)
        {
           DB::rollback();
            return false;
        }

        DB::commit();
        return true;
    }
}


if(!function_exists('obtener_nivel_por_premios'))
{
    function obtener_nivel_por_premios()
    {
        //Obtener los premios
        $premios = contador_premios();
        foreach($premios as $premio)
        {
            if($premio->quantity > 0)
            {
                return $premio->level;
            }
        }
    }
}
