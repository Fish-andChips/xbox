<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    public function weekly_rewards()
    {
    	return $this->hasMany('App\WeeklyReward');
    }
}
