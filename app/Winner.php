<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    protected $fillable = [ 'user_id', 'ticket_id', 'reward_id', 'reward_name', 'week_id'];
    public function ticket()
    {
    	return $this->belongsTo('App\Ticket');
    }
}
