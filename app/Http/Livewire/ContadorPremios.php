<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ContadorPremios extends Component
{
    public $premios;

    public function mount()
    {
        $this->premios = contador_premios();

        //Cantidad de xobx totales que restan
        //$xbox_qty = \DB::table('weekly_rewards')->select(\DB::raw('count(id) as total'))->where('reward_id', '=', 4)->where('quantity', '>', 0)->first();
        //dd($xbox_qty);
        $this->premios[3]->quantity = 3;
        //dd($this->premios[3]);
    }

    public function render()
    {
        return view('livewire.contador-premios');
    }
}
