<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Ticket;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class Tickets extends Component
{

	public $ticket;
	public $estado;
	public $ciudad;
	public $sucursal;

    public $ticket_activo, $ciudad_activo, $sucursal_activo, $error_ticket_activo, $guardando = false;

	public $estados, $ciudades, $sucursales = [];

    public function updatedTicket()
    {
        $this->validate(['ticket' => 'min:3']);
    }

	public function mount()
	{
        if(session('ticket'))
        {
            $this->ticket_activo = true;
        }

        $this->obtenerEstados();
	}

	public function registrar()
	{
        if($this->ticket_activo)
        {
            return;
        }

		$this->validate([
            'estado' => ['required', 'gt:0'],
            'ciudad' => ['required', 'gt:0'],
            'sucursal' => ['required', 'gt:0'],
            'ticket' => ['required', Rule::unique('tickets')->where(function($query){
            	return $query->where('store', $this->sucursal);
            }) ],
        ]);

        //Obtener los premios a repartir para asignar el nivel del ticket
        $nivel_inicial = obtener_nivel_por_premios();
        //dd($nivel_inicial);

        $ticket = Ticket::create([
            'ticket' => $this->ticket,
            'user_id' => Auth::user()->id,
            'store' => $this->sucursal,
            'level' => $nivel_inicial,
            'week_id' => current_week()
        ]);

        session(['ticket' => $ticket->id, 'nivel' => $nivel_inicial]);

        session()->flash('titulo', 'Registro de ticket');
        session()->flash('message', 'Tu ticket con número ' .  $this->ticket . ' ha sido registrado correctamente');

        $this->resetForm();
        redirect()->intended(route('trivias'));

    }

    public function resetForm()
    {
        $this->ciudades = null;
        $this->sucursales = null;
        $this->estado = null;
        $this->ciudad = null;
        $this->sucursal = null;
        $this->ticket = '';
        $this->emit('clearForm');
    }


    public function render()
    {
    	if(!empty($this->estado))
    	{
    		$this->obtenerCiudades();
    	}

    	if(!empty($this->ciudad))
    	{
    		$this->obtenerSucursales();
    	}

        return view('livewire.tickets');
    }

    private function obtenerEstados()
    {
        $db_estados = \DB::connection('mysql_ext')->table('core_states')->get();
		foreach ($db_estados as $estado_db) {
			$this->estados[$estado_db->id] = $estado_db->state;
		}
    }

    private function obtenerCiudades()
    {
        $this->ciudad_activo = false;
        $this->sucursal_activo = false;
    	$this->ciudades = [];
    	$this->sucursales = [];

		$db_ciudades = \DB::connection('mysql_ext')->table('core_stores')
							->select('core_cities.id', 'core_cities.city')
							->join('core_cities', 'core_stores.core_city_id', '=', 'core_cities.id')
							->where('core_stores.core_state_id', $this->estado)
							->distinct()
							->get();

		foreach ($db_ciudades as $db_ciudad) {
			$this->ciudades[$db_ciudad->id] = $db_ciudad->city;
        }
        $this->ciudad_activo = true;
    }

    private function obtenerSucursales()
    {
    	$this->sucursales = [];

    	$db_sucursales = \DB::connection('mysql_ext')->table('core_stores')
    						->select('id', 'store')
    						->where('core_state_id', $this->estado)
    						->where('core_city_id', $this->ciudad)
    						->get();
    	foreach ($db_sucursales as $db_sucursal) {
			$this->sucursales[$db_sucursal->id] = $db_sucursal->store;
        }

        $this->sucursal_activo = true;
    }
}
