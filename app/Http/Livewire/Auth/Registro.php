<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;

class Registro extends Component
{
    public function render()
    {
        return view('livewire.auth.registro');
    }
}
