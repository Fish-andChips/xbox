<?php

namespace App\Http\Livewire\Auth\User;

use Livewire\Component;
use Carbon\Carbon;

use App\User;

class Profile extends Component
{
    public $updated = false;
    public $usuario = [];
    public $avatar = false;


	public function mount()
	{
		$this->asignarDatos(auth()->user());
	}

    public function render()
    {
        return view('livewire.auth.user.profile');
    }

    private function asignarDatos($user)
    {
        //dd($user);
        $this->avatar = $user->avatar;
    	$this->usuario['nombre'] = ($user->name) ? $user->name : '';
    	$this->usuario['apellido'] = ($user->lastname) ? $user->lastname : '';
    	$this->usuario['fecha_nacimiento'] = ($user->birth_date) ? Carbon::parse($user->birth_date)->format('d/m/Y') : '';
    	$this->usuario['telefono'] = ($user->phone_number) ? $user->phone_number : '';
    	$this->usuario['ciudad'] = ($user->city) ? $user->city : '';
    	$this->usuario['codigo_postal'] = ($user->postal_code) ? $user->postal_code : '';
    	$this->usuario['email'] = $user->email;
    }
}
