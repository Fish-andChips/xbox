<?php

namespace App\Http\Livewire\Auth\User;

use Livewire\Component;

class ProfileMenu extends Component
{
    public $avatar = false;

    public function mount()
    {
        $this->avatar = auth()->user()->avatar;
    }

    public function render()
    {
        return view('livewire.auth.user.profile-menu');
    }
}
