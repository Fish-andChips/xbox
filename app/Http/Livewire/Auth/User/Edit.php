<?php

namespace App\Http\Livewire\Auth\User;

use Livewire\Component;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class Edit extends Component
{
    public $updated = false;
    public $nombre, $apellido, $telefono, $email, $fecha_nacimiento, $codigo_postal, $ciudad;
    public $password = '********';
    public $avatar = false;


	public function mount()
	{
		$this->asignarDatos(auth()->user());
    }

    /** Validaciones en tiempo real */
    public function updatedEmail()
    {
        $this->validate([
            'email' => ['required','email', 'unique:users,email,' . auth()->user()->id . ',id']
        ]);
    }
    public function updatedCodigoPostal()
    {
        $this->validate([ 'codigo_postal' => 'digits:5']);
    }
    public function updatedTelefono()
    {
        $this->validate(['telefono' => 'digits:10']);
    }

	public function updateProfile()
	{
		$data = $this->validate([
            'nombre' => ['required'],
            'apellido' => ['required'],
            'ciudad' => ['required'],
            'codigo_postal' => ['required', 'digits:5'],
            'email' => ['required','email', 'unique:users,email,' . auth()->user()->id . ',id'],
            'password' => ['required','min:8'],
            'fecha_nacimiento' => ['date_format:d/m/Y', 'nullable'],
            'telefono' => ['digits:10']
        ]);

		$user = auth()->user();

		$user->name = $data['nombre'];
		$user->lastname = $data['apellido'];
		$user->birth_date = Carbon::createFromFormat('d/m/Y', $data['fecha_nacimiento']);
		$user->phone_number = $data['telefono'];
		$user->city = $data['ciudad'];
		$user->postal_code = $data['codigo_postal'];
        $user->email = $data['email'];

        if($data['password'] != '********' )
            $user->password = Hash::make($data['password']);

        $user->save();

        session()->flash('titulo', 'Perfil');
        session()->flash('message', 'Tus datos se han actualizado');

        redirect()->intended(route('perfil'));

	}

    private function asignarDatos($user)
    {
    	$this->avatar = $user->avatar;
    	$this->nombre = ($user->name) ? $user->name : '';
    	$this->apellido = ($user->lastname) ? $user->lastname : '';
    	$this->fecha_nacimiento = ($user->birth_date) ? Carbon::parse($user->birth_date)->format('d/m/Y') : '';
    	$this->telefono = ($user->phone_number) ? $user->phone_number : '';
    	$this->ciudad = ($user->city) ? $user->city : '';
    	$this->codigo_postal = ($user->postal_code) ? $user->postal_code : '';
    	$this->email = $user->email;
    }

    public function render()
    {
        return view('livewire.auth.user.edit');
    }
}
