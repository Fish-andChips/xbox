<?php

namespace App\Http\Livewire\Auth;

use App\Providers\RouteServiceProvider;
use Exception;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Verify extends Component
{
    public function resend()
    {
        if (Auth::user()->hasVerifiedEmail()) {
            redirect(route('home'));
        }

        $response = true;

        try
        {
            Auth::user()->sendEmailVerificationNotification();
        }
        catch(Exception $e)
        {
            $response = false;
        }

        if($response)
        {
            $this->emit('resent');
            session()->flash('resent');
        }
        else
        {
            session()->flash('error_send');
        }

    }

    public function render()
    {
        return view('livewire.auth.verify');
    }
}
