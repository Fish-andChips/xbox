<?php

namespace App\Http\Livewire\Trivias;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

use App\Ticket;

class Principal extends Component
{
    public $ticket, $nivel, $no_ticket = false;
    public $concursantes;

	public function mount()
	{
        if(!session('ticket'))
        {
            $this->no_ticket = true;
        }

		//Buscar ticket
        $this->ticket = session('ticket');
        $this->nivel = session('nivel');
	}

    public function render()
    {
        return view('livewire.trivias.principal');
    }
}

