<?php

namespace App\Http\Livewire\Trivias;

use Livewire\Component;

class Concursantes extends Component
{
    public $concursantes;

    public function mount()
    {
        $this->concursantes = rand(200,600);
    }

    public function render()
    {
        return view('livewire.trivias.concursantes');
    }
}
