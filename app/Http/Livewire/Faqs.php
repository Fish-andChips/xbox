<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Faq;

class Faqs extends Component
{
	public $faqs = [];

	public function mount()
	{
		$faqsDB = Faq::orderBy('id', 'ASC')->get();
		foreach ($faqsDB as $faq) {
			$this->faqs[$faq->question] = $faq->answer;
		}

		//dd($faqs);
	}

    public function render()
    {
        return view('livewire.faqs');
    }
}
