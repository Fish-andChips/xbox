<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PoliticaPrivacidad extends Component
{
    public function render()
    {
        return view('livewire.politica-privacidad');
    }
}
