<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GanadorGamepassemail extends Mailable
{
    use Queueable, SerializesModels;

    public $nivel;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nivel)
    {
        $this->nivel = $nivel;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        switch($this->nivel)
        {
            case 1: return $this->from('contacto@datelavueltaconxbox.com')->view('mails.ganador_gamepass_14');
            case 2: return $this->from('contacto@datelavueltaconxbox.com')->view('mails.ganador_gamepass_mes');
            case 3: return $this->from('contacto@datelavueltaconxbox.com')->view('mails.ganador_gamepass_3meses');
        }
    }
}
