<?php

namespace App\Listeners;

use App\Events\EmailGanadorUsuario;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\GanadorXboxEmail;
use App\Mail\GanadorGamepassemail;

use Mail;

class EnviarCorreoGanador
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmailGanadorUsuario  $event
     * @return void
     */
    public function handle(EmailGanadorUsuario $event)
    {
        $nivel = $event->nivel;
        $to = $event->email;

        if($nivel < 4)
            Mail::to($to)->send(new GanadorGamepassEmail($nivel));
        else
            Mail::to($to)->send(new GanadorXboxEmail());

    }
}
