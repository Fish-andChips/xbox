<?php

namespace App\Listeners;

use App\Events\EmailPerdedorUsuario;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\PerdedorEmail;

use Mail;

class EnviarCorreoPerdedor
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmailPerdedorUsuario  $event
     * @return void
     */
    public function handle(EmailPerdedorUsuario $event)
    {
        $nivel = $event->nivel;
        $to = auth()->user()->email;
        Mail::to($to)->send(new PerdedorEmail());
    }
}
