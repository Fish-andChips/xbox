function progress(timeleft, timetotal, $element) {
    var progressBarWidth = (timetotal-timeleft) * ($element.width()/timetotal);
    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
    if(timeleft <= 5)
    {
        $('#i_bomb').removeClass('text-x_green_lightest').addClass('text-red-500');
        $('#i_explosion').removeClass('text-x_green_lightest').addClass('text-red-500');
        $element.find('div').removeClass('bg-x_green_lightest').addClass('bg-red-500');
    }

    if(timeleft > 0) {
        setTimeout(function() {
            progress(timeleft - 1, timetotal, $element);
        }, 1000);
    }
};

progress(15, 15, $('#barra_tiempo'));
