module.exports = {
    theme: {
        extend: {
            colors: {
                x_green_lightest: '#00d500',
                x_green_light: '#24A112',
                x_green: '#186608',
                x_dark: '#1d1d1d',
                x_blue: '#1877F2',
                x_modal_semi: 'rgba(0,0,0,0.75)',
                x_red: '#EE3D42'
            },
            fontFamily: {
                arial: ['Arial', 'sans-serif'],
                narrow: ['Arial\\ Narrow', 'sans-serif'],
                amsi: ['AmsiPro', 'Arial', 'sans-serif'],
                industry: ['IndustryBlack', 'Arial', 'sans-serif']
            },
        },
        fontSize: {
            'xs': '.75rem',
            'sm': '.875rem',
            'tiny': '.875rem',
            'base': '1rem', // 16px
            'lg': '1.125rem',
            'xl': '1.25rem',
            '2xl': '1.5rem', // 24px
            '3xl': '2rem', // 32px
            '4xl': '2.5rem', // 40px
            '5xl': '3rem', //48px
            '6xl': '4rem',
            '7xl': '5rem',
        }
    },
    variants: {},
    purge: {
        content: [
            './app/**/*.php',
            './resources/**/*.html',
            './resources/**/*.js',
            './resources/**/*.jsx',
            './resources/**/*.ts',
            './resources/**/*.tsx',
            './resources/**/*.php',
            './resources/**/*.vue',
            './resources/**/*.twig',
        ],
        options: {
            defaultExtractor: (content) => content.match(/[\w-/.:]+(?<!:)/g) || [],
            whitelistPatterns: [/-active$/, /-enter$/, /-leave-to$/, /show$/],
        },
    },
    plugins: [
    ],
};
