<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RewardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $premios = [
        	"1" => "Game pass 14 días",
        	"2" => "Game pass 1 mes",
        	"3" => "Game pass 3 meses",
        	"4" => "xbox one x"
        ];

        foreach ($premios as $nivel => $premio) {
        	DB::table('rewards')->insert([
        		'reward' => $premio,
        		'level' => $nivel,
        		'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        	]);
        }
    }
}
