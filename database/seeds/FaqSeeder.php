<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	//Array de preguntas y respuestas
    	$faqs = [
    		"¿Cómo puedo ganar un premio de XBOX?" => "Hola, para participar es necesario visitar tu sucursal favorita y comprar un Blizzard® chico, mediano o grande, en la manga promocional encontrarás un código QR que podrás escanear para ingresar tu ticket de compra y responder las trivias que te permitirán concursar por uno de nuestros premios.",
    		"¿Hasta cuándo tengo para participar?" => "Hola , esta promoción es válida desde el 17 de Agosto y hasta del 27 de Septiembre del 2020 en todas las sucursales de DQ® México.",
    		"¿Qué tengo que hacer para ganar?" => "Hola, para ganar es necesario que acudas a tu tienda DQ® más cercana y compres uno de nuestros productos participantes, donde podrás encontrar una manga para registrar tu ticket y comenzar a responder las trivias correctamente.",
    		"¿Qué premios puedo ganar?" => "Tenemos 18,000 premios disponibles para ti, desde un Game Pass por 14 días, o Game Pass por 1 mes, Game Pass de 3 meses y hasta una consola XBOX ONE X.",
    		"¿Participan los productos comprados por delivery?" => "Hola, así es, todos los Blizzard® en chico, mediano y grande participan.",
    		"¿Cuántas oportunidades tengo de participar con mi ticket?" => "Hola,  tu ticket te permitirá avanzar en las trivias hasta que contestes de manera incorrecta solo una vez, para volver a participar en las trivias deberás registrar un nuevo ticket. ",
    		"¿Cada cuánto se anunciará n los ganadores?" => "Hola, los ganadores se anunciarán semanalmente vía correo electrónico.",
    		"¿Los premios son acumulables?" => "Hola, cada ticket te permitirá participar una vez y contestar correctamente hasta ganar un premio, si quieres participar nuevamente deberás registrar otro ticket. ",
    		"¿Qué pasa si contesto la trivia incorrectamente?" => "Perderás la oportunidad de seguir participando, y tendrás que registrar un nuevo ticket y comenzar las trivias desde 0.",
    		"¿Tengo que guardar mi ticket?" => "Hola, no es necesario guardar físicamente tu ticket, pero te pedimos que le tomes una fotografía para así comprobar que eres el ganador a la hora de reclamar tu premio.",
    		"¿Qué pasa si registro mal mi ticket?" => "Hola, tu ticket quedará bloqueado y deberás registrar uno nuevo.",
    		"¿Puedo ganar más de una vez?" => "Hola, claro que sí, cada ticket que registres será una nueva oportunidad de participar y ganar.",
    		"¿Puedo participar con otros productos?" => "Hola, únicamente los tickets con los productos participantes podrán acceder a los premios. ",
    		"¿En dónde registro mi ticket?" => "Debes registrar tu ticket en www.datelavueltaconxbox.com y tomarle una foto para reclamar tu premio en caso de ser ganador.",
    		"¿Si mi blizzard® no tenía manga puedo participar?" => "Hola, si tu Blizzard® es chico, mediano, o grande es válido para participar en la dinámica del 17 de Agosto hasta el 27 de Septiembre del 2020, aún cuando el producto no cuente con una manga. Puedes escanear el código QR en tu tienda DQ® o ingresar directamente a www.datelavueltaconxbox.com",
    		"¿Los ganadores se eligen al azar?" => "Hola, no. Para ganar deberás responder una serie de trivias que irán aumentando su nivel de dificultad y en caso de responder correctamente podrás avanzar hasta ganar uno de nuestros 18 Mil premios.",
    		"¿Cuál es la vigencia de mi Ticket?" => "Hola, puedes registrar cualquier ticket de compra con productos participantes expedido entre el 17 de Agosto hasta el 27 de Septiembre del 2020. ",
    		"¿Puedo participar en la dinámica si soy empleado de DQ®?" => "Hola, no. La dinámica es exclusiva para clientes de DQ® y personas externas a la empresa.",
    		"¿Donde puedo llamar o contactarlos si tengo alguna duda o algún problema?" => "Hola, nos puedes contactar vía correo electrónico a no-reply@datelavueltaconxbox.com o a través de mensaje en nuestras redes sociales ¡Con gusto atenderemos tus dudas en un horario de L-V de 9am-7pm!                                ",
    		"¿Qué sucede si por error ingresé un número diferente al de mi ticket y resulté ganador?" => "Hola, debes asegurarte de registrar correctamente el número de tu ticket de compra antes de comenzar las trivias. En caso de ser ganador; si el número de tu ticket no coincide con el registrado, no podrás hacer válido tu premio.",
    		"¿Qué pasa si al tratar de registrar mi ticket aparece que ya no es válido?" => "Hola, recuerda que cada ticket de compra es igual a una única participación en nuestras trivias. Y sólo participarán tickets expedidos del 17 de Agosto de 2020 al 27 de Septiembre de 2020."
    	];

    	foreach($faqs as $question => $answer)
		{
			DB::table('faqs')->insert([
        		'question' => $question,
        		'answer' => $answer,
        		'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        	]);
		}

    }
}
