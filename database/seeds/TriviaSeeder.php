<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TriviaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trivias = [
            "1" => [
                "1" => [
                    [
                        "pregunta" => "¿Quién es el protagonista de los principales títulos de la saga de Halo? ",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Cortana", "2" => "Marcus Phoenix", "3" => "Master Chief"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el nombre del protagonista de Gears 5?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Marcus Phoenix", "2" => "Kait Díaz"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el producto estrella de Dairy Queen®?",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => ["1" => "Banana Split", "2" => "Cono", "3" => "Blizzard®"]
                    ],
                    [
                        "pregunta" => "¿Qué incluye la membresía a Xbox Game Pass Ultimate?",
                        "correcta" => "2",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Acceso a juego en línea y descuentos exclusivos", "2" => "Xbox Game Pass para consola, Xbox Game Pass para PC y Xbox Live Gold", "3" => "Acceso a todos los juegos de Xbox Game Studios"]
                    ],
                    [
                        "pregunta" => "¿Cómo se le conoce a tu nombre de usuario de Xbox Live?",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Handle", "2" => "Nametag", "3" => "Gamertag"]
                    ],
                    [
                        "pregunta" => "¿Cuál de éstas no es una presentación de Blizzard®? ",
                        "correcta" => "1",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Extra Grande", "2" => "Mini", "3" => "Familiar"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el nombre de la membresía de Xbox que te permite jugar en línea con tus amigos, además de incluir descuentos exclusivos y hasta cuatro juegos gratuitos al mes?",
                        "correcta" => "3",
                        "limite" => "25",
                        "respuestas" => [ "1" => "Xbox Live", "2" => "Xbox Game Pass", "3" => "Xbox Live Gold"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el nombre del servicio de Xbox que te permite jugar tus títulos digitales tanto en tu consola como en Windows PC?",
                        "correcta" => "2",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Xbox Game Pass", "2" => "Xbox Play Anywhere", "3" => "Xbox Better Together"]
                    ]
                ],
                "2" => [
                    [
                        "pregunta" => "¿Cuál es el nombre completo del primer título publicado de la saga Halo?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Halo: The Master Chief’s Adventure", "2" => "Halo: Combat Evolved", "3" => "Halo: Reach"]
                    ],
                    [
                        "pregunta" => "En 2020 Dairy Queen® celebra su aniversario número__",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "100", "2" => "80", "3" => "60"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el nombre del accesorio de Xbox que a través de una cámara sensor de movimiento permitía que tú fueras el control?",
                        "correcta" => "2",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Camera X", "2" => "Kinnect", "3" => "Net Move"]
                    ],
                    [
                        "pregunta" => "¿Cuál fue el primer título lanzado por la Franquicia de Forza?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Forza Horizon", "2" => "Forza Motorsport", "3" => "Forza Racing Championship"]
                    ],
                    [
                        "pregunta" => "¿De qué color es la icónica cuchara de nuestros Blizzard®?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Verde", "2" => "Rojo", "3" => "Azul"]
                    ],
                    [
                        "pregunta" => "¿Cómo se llama el juego para móviles de la franquicia de Gears?",
                        "correcta" => "1",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Gears Pop!", "2" => "Gears of War Judgment", "3" => "Gears Go!"]
                    ],
                    [
                        "pregunta" => "¿Cómo se llama el juego de realidad aumentada para móviles de la franquicia Minecraft?",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Gears Dungeons", "2" => "Minecraft Go!", "3" => "Minecraft Earth"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el sabor de Blizzard® más vendido en el mundo?",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Chocolate Extreme", "2" => "Rojo", "Fresa Pay de Queso" => "Oreo®"]
                    ]
                ],
                "3" => [
                    [
                        "pregunta" => "¿Cómo se distinguen los conos de Dairy Queen®?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Por su color", "2" => "Por el rizo", "3" => "Por la cobertura"]
                    ],
                    [
                        "pregunta" => "¿Cómo se le conoce al personaje o avatar masculino y femenino de Minecraft? ",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Bob y Jenny", "2" => "Steve y Alex", "3" => "Mark y Sam"]
                    ],
                    [
                        "pregunta" => "¿En qué año se lanzó la primera consola de Xbox?",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => [ "1" => "1999", "2" => "2000", "3" => "2001"]
                    ],
                    [
                        "pregunta" => "¿Para cuántas personas es un Pastel DQ® rectangular?",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => [ "1" => "8 a 10 personas", "2" => "12 a 17 personas", "3" => "20 a 25 personas"]
                    ],
                    [
                        "pregunta" => "¿Cuál fue el nombre clave de Xbox Series X antes de su anuncio oficial?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Project Gama", "2" => "Project Scarlett", "3" => "Project X"]
                    ],
                    [
                        "pregunta" => "¿En qué año se lanzó el Xbox 360?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "2003", "2" => "2005", "3" => "2008"]
                    ],
                    [
                        "pregunta" => "El DQ® Freeze es una deliciosa combinación de__",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "DQ® Slush con leche", "2" => "Helado de Vainilla con DQ® Slush", "3" => "Yogurt con DQ® Slush"]
                    ],
                    [
                        "pregunta" => "¿Quién es el Head of Xbox o Director de Xbox?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Satya Nadella", "2" => "Phil Spencer", "3" => "Bill Gates"]
                    ]
                ],
                "4" => [
                    [
                        "pregunta" => "¿Cuál es el nombre del estudio que desarrolló los primeros títulos de la franquicia de Halo?",
                        "correcta" => "3",
                        "limite" => "20",
                        "respuestas" => [ "1" => "The Coalition", "2" => "Rare", "3" => "Bungie"]
                    ],
                    [
                        "pregunta" => "¿Cuál fue el nombre clave de Kinect antes de su anuncio oficial?",
                        "correcta" => "1",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Project Natal", "2" => "Project Movement", "3" => "Project Revolution"]
                    ],
                    [
                        "pregunta" => "¿En qué año fue lanzado Xbox Live?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "2001", "2" => "2002", "3" => "2005"]
                    ],
                    [
                        "pregunta" => "Los Creepers de Minecraft les temen a estas dos criaturas:",
                        "correcta" => "1",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Gatos y Ocelotes", "2" => "Esqueletos y Arañas", "3" => "Zombies y Enderman"]
                    ],
                    [
                        "pregunta" => "¿En cuántos estados de la República Mexicana hay tiendas de Dairy Queen®?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "25 Estados", "2" => "29 Estados", "3" => "31 Estados"]
                    ],
                    [
                        "pregunta" => "¿Cuántas veces se ha realizado Xbox FanFest Ciudad de México?",
                        "correcta" => "1",
                        "limite" => "15",
                        "respuestas" => [ "1" => "5", "2" => "6", "3" => "8"]
                    ],
                    [
                        "pregunta" => "¿Durante qué evento fue la última vez que Phil Spencer, Head of Xbox, visitó la ciudad de México?",
                        "correcta" => "2",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Evento de lanzamiento de Gears 5 en México", "2" => "Xbox FanFest Ciudad de México 2019 / X019", "3" => "Gears Pro Circuit 2019"]
                    ],
                    [
                        "pregunta" => "¿Qué modelo de automóvil fue presentado en la conferencia de Xbox de E3 2018?",
                        "correcta" => "1",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Porsche 911 GT2 RS", "2" => "McLaren Senna", "3" => "DeLorean DMC-12"]
                    ]
                ]
            ],
            "2" => [
                "1" => [
                    [
                        "pregunta" => "¿Cómo se llama la paleta de Helado con cobertura sabor chocolate de DQ®?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Magic Bar", "2" => "Dilly Bar", "3" => "Choco Paleta"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el nombre del control más avanzado del mundo y compatible con Xbox One y Windows 10?",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Xbox Pro Controller", "2" => "Xbox Esports Controller", "3" => "Xbox Elite Series 2"]
                    ],
                    [
                        "pregunta" => "¿Cómo incrementa el GamerScore?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => ["1" => "Jugando títulos de Xbox Game Pass", "2" => "Al desbloquear logros de los títulos de Xbox", "3" => "Participando en eventos para comunidad"]
                    ],
                    [
                        "pregunta" => "¿De qué sabor es el suave Helado de Dairy Queen®?",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Yogurt natural", "2" => "Chocolate", "3" => "Vainilla"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el nombre real de Master Chief?",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Adam – 177 ", "2" => "John Conor", "3" => "John-117"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el nombre del padre de Kait Díaz de la saga de Gears?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Adam Díaz ", "2" => "Gabe Díaz", "3" => "Dominic Díaz "]
                    ],
                    [
                        "pregunta" => "¿En qué ciudad nació DQ®?",
                        "correcta" => "1",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Illinois", "2" => "Ciudad de México", "3" => "París"]
                    ],
                    [
                        "pregunta" => "Menciona todos los modelos de consolas Xbox que han sido lanzadas al día de hoy:",
                        "correcta" => "1",
                        "limite" => "25",
                        "respuestas" => [ "1" => "Xbox, Xbox 360, Xbox 360 S, Xbox 360 E, Xbox One, Xbox One S, Xbox One X y Xbox One S All- Digital.", "2" => "Xbox, Xbox 360, Xbox 360 S, Xbox 360 E, Xbox One, Xbox One S, Xbox One X, Xbox One S All- Digital y Xbox Series X.", "3" => "Xbox, Xbox 360, Xbox One, Xbox One S, Xbox One X, Xbox One S All- Digital y Xbox Series X."]
                    ]
                ],
                "2" => [
                    [
                        "pregunta" => "¿Cómo se llama el juego móvil de la franquicia de Forza?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Forza Motorsport", "2" => "Forza Street", "3" => "Forza Pocket"]
                    ],
                    [
                        "pregunta" => "¿En qué año se lanzó Halo: Combat Evolved?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "1999", "2" => "2001", "3" => "2003"]
                    ],
                    [
                        "pregunta" => "¿Cómo recibes tu Blizzard®?",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Con tapa", "2" => "Sin tapa", "3" => "Al revés"]
                    ],
                    [
                        "pregunta" => "¿Quién es el actor de doblaje oficial de Marcus Phoenix de la saga de Gears para LATAM?",
                        "correcta" => "2",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Mario Castañeda", "2" => "Sebastián Llapur", "3" => "Humberto Vélez"]
                    ],
                    [
                        "pregunta" => "¿Cuántos títulos de la saga de Forza han sido publicados?",
                        "correcta" => "2",
                        "limite" => "20",
                        "respuestas" => [ "1" => "4 de la saga Street, 7 de Horizon y Forza Horizon", "2" => "4 de la saga Horizon, 7 de Motorsport y Forza Street", "3" => "4 de la saga Horizon, 7 de Street y Forza Motorsport"]
                    ],
                    [
                        "pregunta" => "¿En qué meses se ha celebrado El Día Del Cono Gratis?",
                        "correcta" => "1",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Marzo y Abril", "2" => "Abril y Mayo", "3" => "Febrero y Marzo"]
                    ],
                    [
                        "pregunta" => "¿Cuál fue el nombre clave de Xbox One X antes de su anuncio oficial?",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => [ "1" => "Project Dolphin", "2" => "Project Cortana", "3" => "Project Scorpio"]
                    ],
                    [
                        "pregunta" => "¿En qué año se lanzó Xbox Game Pass?",
                        "correcta" => "3",
                        "limite" => "15",
                        "respuestas" => [ "1" => "2001", "2" => "2015", "3" => "2017"]
                    ]
                ],
                "3" => [
                    [
                        "pregunta" => "¿En qué ciudad se encuentran las oficinas corporativas de Dairy Queen®?",
                        "correcta" => "1",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Minneapolis, Minnesota", "2" => "Chicago, Illinois", "3" => "Dallas, Texas"]
                    ],
                    [
                        "pregunta" => "¿Cuántos candidatos tenía el Programa SPARTAN-II en el que participo John-117 durante su infancia?",
                        "correcta" => "1",
                        "limite" => "20",
                        "respuestas" => [ "1" => "150 candidatos", "2" => "130 candidatos", "3" => "360 candidatos"]
                    ],
                    [
                        "pregunta" => "¿Qué Easter Egg se esconde dentro del Xbox One X?",
                        "correcta" => "3",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Marcus Phoenix peleando con un escorpión", "2" => "Un escorpión", "3" => "Master Chief montando un escorpión"]
                    ],
                    [
                        "pregunta" => "Estos dos títulos fueron desarrollados originalmente para Windows Phone y IOS",
                        "correcta" => "2",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Halo ODST & Halo Assault", "2" => "Spartan Assault & Spartan Strike", "3" => "Halo Infinite & Halo Odyssey"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el nombre del estudio que actualmente desarrolla la franquicia de Halo, incluyendo su próximo lanzamiento Halo Infinite?",
                        "correcta" => "2",
                        "limite" => "20",
                        "respuestas" => [ "1" => "The Coalition", "2" => "343 Industries", "3" => "Turn 10 Studios"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el nombre del estudio que actualmente desarrolla la franquicia de Gears?",
                        "correcta" => "1",
                        "limite" => "20",
                        "respuestas" => [ "1" => "The Coalition", "2" => "343 Industries", "3" => "Turn 10 Studios"]
                    ],
                    [
                        "pregunta" => "¿Cuántos sabores de café caliente puedes encontrar en DQ®?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "4 sabores", "2" => "Ninguno", "3" => "8 sabores"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el nombre del estudio que actualmente desarrolla la franquicia de Minecraft?",
                        "correcta" => "3",
                        "limite" => "20",
                        "respuestas" => [ "1" => "The Coalition", "2" => "Turn 10 Studios", "3" => "Mojang"]
                    ]
                ],
                "4" => [
                    [
                        "pregunta" => "¿Durante qué evento fue anunciado el primer Xbox?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "E3 2000", "2" => "GDC 2000", "3" => "Space World 2002"]
                    ],
                    [
                        "pregunta" => "¿Qué figura acompañó a Bill Gates durante la presentación del primer Xbox?",
                        "correcta" => "1",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Dwyane “The Rock” Johnson", "2" => "John Cena", "3" => "Keanu Reeves"]
                    ],
                    [
                        "pregunta" => "¿Quién fue el fundador de Dairy Queen®?",
                        "correcta" => "2",
                        "limite" => "15",
                        "respuestas" => [ "1" => "James McLamore", "2" => "John Fremont McCullough", "3" => "Karl Karcher"]
                    ],
                    [
                        "pregunta" => "¿Qué título de la franquicia de Minecraft puede disfrutarse a través de Netflix?",
                        "correcta" => "3",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Gears Dungeons", "2" => "Minecraft Earth", "3" => "Minecraft Story mode"]
                    ],
                    [
                        "pregunta" => "Los Creepers de Minecraft se basaron en el modelo fallido del siguiente Mob o Creatura:",
                        "correcta" => "1",
                        "limite" => "20",
                        "respuestas" => [ "1" => "Cerdo", "2" => "Gato", "3" => "Ocelote"]
                    ],
                    [
                        "pregunta" => "¿Cuál es el Gamertag del Gamer mexicano con el mayor GamerScore del país?",
                        "correcta" => "1",
                        "limite" => "15",
                        "respuestas" => [ "1" => "SirDraven", "2" => "Identivez", "3" => "Aarongreenberg"]
                    ],
                    [
                        "pregunta" => "En Xbox FanFest Ciudad de México 2016, este miembro del equipo Xbox fue disfrazado al evento con un mameluco blanco, mismo que fue elegido por la comunidad:",
                        "correcta" => "2",
                        "limite" => "25",
                        "respuestas" => [ "1" => "Major Nelson", "2" => "Aaron Greenberg", "3" => "Bill Gates"]
                    ],
                    [
                        "pregunta" => "Este carismático actor robó los suspiros de los gamers durante la presentación de Cyberpunk 2077 en la conferencia de Xbox de E3 2019:",
                        "correcta" => "1",
                        "limite" => "25",
                        "respuestas" => [ "1" => "Keanu Reeves", "2" => "Henry Cavill", "3" => "Dwyane “The Rock” Johnson"]
                    ],
                ]
            ]
        ];

        foreach($trivias as $semana => $nivel)
        {
            foreach($nivel as $level => $trivia_arr)
            {
                foreach($trivia_arr as $trivia)
                {
                    DB::table('trivias')->insert([
                        'week' => $semana,
                        'level' => $level,
                        'question' => $trivia['pregunta'],
                        'correct' => $trivia['correcta'],
                        'time_limit' => $trivia['limite'],
                        'answers' => json_encode($trivia['respuestas']),
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);
                }
            }
        }

    }
}
