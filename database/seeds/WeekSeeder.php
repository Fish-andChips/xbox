<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class WeekSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $weeks = [
        	"1" => [ 'inicio' => '2020-08-15', 'fin' => '2020-08-23'],
        	"2" => [ 'inicio' => '2020-08-24', 'fin' => '2020-08-30'],
            "3" => [ 'inicio' => '2020-08-31', 'fin' => '2020-09-06'],
            "4" => [ 'inicio' => '2020-09-07', 'fin' => '2020-09-13'],
            "5" => [ 'inicio' => '2020-09-14', 'fin' => '2020-09-20'],
            "6" => [ 'inicio' => '2020-09-21', 'fin' => '2020-09-27']
        ];

        foreach ($weeks as $week => $content) {
        	DB::table('weeks')->insert([
        		'week_number' => $week,
        		'start_date' => $content['inicio'],
        		'end_date' => $content['fin'],
        		'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        	]);
        }
    }
}
