<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(WeekSeeder::class);
        $this->call(RewardSeeder::class);
        $this->call(WeeklyRewardSeeder::class);
        $this->call(TriviaSeeder::class);
        $this->call(FaqSeeder::class);
    }
}
